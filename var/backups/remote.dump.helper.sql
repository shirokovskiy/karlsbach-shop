UPDATE `core_config_data` SET `value` = 'http://shop.karlsbach.du/' WHERE `config_id` IN (6,7) AND `value` LIKE 'http://shop.karlsbach.eu/';
UPDATE `core_config_data` SET `value` = 'http://market.karlsbach.du/' WHERE `value` LIKE 'http://market.karlsbach.eu/';
TRUNCATE TABLE `log_url`;
TRUNCATE TABLE `log_url_info`;
TRUNCATE TABLE `log_visitor`;
TRUNCATE TABLE `log_visitor_info`;
TRUNCATE TABLE `log_visitor_online`;
