<?php
/**
 * Phpwebstudio
 *
 * @author      Shirokovskiy Dmitry <info@phpwebstudio.com>
 * @category    Phpwebstudio
 * @package     Phpwebstudio_Checkout
 * @copyright   Copyright (c) 2011 Phpwebstudio. (http://www.phpwebstudio.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shopping cart block
 *
 */
class Phpwebstudio_Warenkorb_Block_Cart extends Mage_Checkout_Block_Cart // Mage_Core_Block_Template //
{
    /**
     * Prepare Quote Item Product URLs
     *
     */
    public function __construct()
    {
        $this->setTemplate("page/html/warenkorb.phtml");
    }
    
    public function drawBlock() {
        echo $this->toHtml();
    }
    
    protected function _toHtml() {
        //echo $this->toHtml();
        return parent::_toHtml();
    }
}

