<?php

/**
 * Newsletter queue resource model
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Newsletter_Model_Resource_Queue extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('newsletter/queue', 'queue_id');
    }

    /**
     * Add subscribers to queue
     *
     * @param Mage_Newsletter_Model_Queue $queue
     * @param array $subscriberIds
     */
    public function addSubscribersToQueue(Mage_Newsletter_Model_Queue $queue, array $subscriberIds)
    {
        if (count($subscriberIds)==0) {
            Mage::throwException(Mage::helper('newsletter')->__('No subscribers selected.'));
        }

        if (!$queue->getId() && $queue->getQueueStatus()!=Mage_Newsletter_Model_Queue::STATUS_NEVER) {
            Mage::throwException(Mage::helper('newsletter')->__('Invalid queue selected.'));
        }

        $adapter = $this->_getWriteAdapter();

        $select = $adapter->select();
        $select->from($this->getTable('newsletter/queue_link'), 'subscriber_id')
            ->where('queue_id = ?', $queue->getId())
            ->where('subscriber_id in (?)', $subscriberIds);

        $usedIds = $adapter->fetchCol($select);
        $adapter->beginTransaction();
        try {
            foreach ($subscriberIds as $subscriberId) {
                if (in_array($subscriberId, $usedIds)) {
                    continue;
                }
                $data = array();
                $data['queue_id'] = $queue->getId();
                $data['subscriber_id'] = $subscriberId;
                $adapter->insert($this->getTable('newsletter/queue_link'), $data);
            }
            $adapter->commit();
        }
        catch (Exception $e) {
            $adapter->rollBack();
        }
    }

    /**
     * Removes subscriber from queue
     *
     * @param Mage_Newsletter_Model_Queue $queue
     */
    public function removeSubscribersFromQueue(Mage_Newsletter_Model_Queue $queue)
    {
        $adapter = $this->_getWriteAdapter();
        try {
            $adapter->delete(
                $this->getTable('newsletter/queue_link'),
                array(
                    'queue_id = ?' => $queue->getId(),
                    'letter_sent_at IS NULL'
                )
            );

            $adapter->commit();
        }
        catch (Exception $e) {
            $adapter->rollBack();
        }
    }

    /**
     * Links queue to store
     *
     * @param Mage_Newsletter_Model_Queue $queue
     * @return Mage_Newsletter_Model_Resource_Queue
     */
    public function setStores(Mage_Newsletter_Model_Queue $queue)
    {
        $adapter = $this->_getWriteAdapter();
        $adapter->delete(
            $this->getTable('newsletter/queue_store_link'),
            array('queue_id = ?' => $queue->getId())
        );

        $stores = $queue->getStores();
        if (!is_array($stores)) {
            $stores = array();
        }

        foreach ($stores as $storeId) {
            $data = array();
            $data['store_id'] = $storeId;
            $data['queue_id'] = $queue->getId();
            $adapter->insert($this->getTable('newsletter/queue_store_link'), $data);
        }
        $this->removeSubscribersFromQueue($queue);

        if (count($stores) == 0) {
            return $this;
        }

        /**
         * Bug fix.
         * If more than 30000 subscribers, then not enough memory
         * because SQL query all records at the same time.
         * Now it fixed.
         */

        $pageSize = 10000;

        $subscribers = Mage::getResourceSingleton('newsletter/subscriber_collection')
            ->addFieldToFilter('store_id', array('in'=>$stores))
            ->useOnlySubscribed()->setPageSize($pageSize);

        $break  = false;
        $page   = 1;
        $iii    = 0;
        while ($break !== true) {
            $iii++;
            $collection = clone $subscribers;
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);

            $subscriberIds = array();

            foreach ($collection as $subscriber) {
                $subscriberIds[] = $subscriber->getId();
            }

            $page++;

            if (count($subscriberIds) > 0) {
                $this->addSubscribersToQueue($queue, $subscriberIds);
            } else
                $break = true;

            if ($iii >= 100 && !$break) {
                $break = true;
                Mage::log('Break Newsletter Queue setting subscribers! More then '.$iii.' iterations!');
            }
        }

        return $this;
    }

    /**
     * Returns queue linked stores
     *
     * @param Mage_Newsletter_Model_Queue $queue
     * @return array
     */
    public function getStores(Mage_Newsletter_Model_Queue $queue)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('newsletter/queue_store_link'), 'store_id')
            ->where('queue_id = :queue_id');

        if (!($result = $adapter->fetchCol($select, array('queue_id'=>$queue->getId())))) {
            $result = array();
        }

        return $result;
    }

    /**
     * Saving template after saving queue action
     *
     * @param Mage_Core_Model_Abstract $queue
     * @return Mage_Newsletter_Model_Resource_Queue
     */
    protected function _afterSave(Mage_Core_Model_Abstract $queue)
    {
        if ($queue->getSaveStoresFlag()) {
            $this->setStores($queue);
        }
        return $this;
    }
}
