<?php

/**
 * @creator - Harish Kumar B P from GlyphWebCore Technologies
 * @website - http://www.magedevelopment.com
 * @module  - GlyphWebCore NewsletterTest
 *
**/

class GlyphWebCore_NewsletterTest_Adminhtml_SendtestController extends Mage_Adminhtml_Controller_Action {

    const XML_PATH_SENDING_SET_RETURN_PATH      = 'system/smtp/set_return_path';
    const XML_PATH_SENDING_RETURN_PATH_EMAIL    = 'system/smtp/return_path_email';

    public function indexAction()
    {
		$this->loadLayout();
        $this->renderLayout();
    }




    public function postAction()
    {
        $post = $this->getRequest()->getPost();

        try
        {
        	if(empty($post))
        	{
                Mage::throwException($this->__('Invalid form data.'));
            }

            ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
            ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

            $template = Mage::getModel('newsletter/template');
            $queue = Mage::getModel('newsletter/queue');
            $queue->load($post['id']);
            $template->setTemplateType($queue->getNewsletterType());
            $template->setTemplateText($queue->getNewsletterText());
            $template->setTemplateStyles($queue->getNewsletterStyles());
            $sender = $queue->getNewsletterSenderEmail();
            $subject = $queue->getNewsletterSubject();

        	$storeId = (int)$this->getRequest()->getParam('store_id');
        	if(!$storeId)
        	{
            	$storeId = Mage::app()->getDefaultStoreView()->getId();
        	}

        	Varien_Profiler::start("newsletter_queue_proccessing");

        	$vars = array();
        	$vars['subscriber'] = Mage::getModel('newsletter/subscriber');

        	$template->emulateDesign($storeId);
        	$templateProcessed = $template->getProcessedTemplate($vars, true);
        	$template->revertDesign();

        	if($template->isPlain())
        	{
            	$templateProcessed = "<pre>" . htmlspecialchars($templateProcessed) . "</pre>";
        	}

        	Varien_Profiler::stop("newsletter_queue_proccessing");

            $mail = new Zend_Mail('utf-8');

            $setReturnPath = Mage::getStoreConfig(self::XML_PATH_SENDING_SET_RETURN_PATH);
            switch ($setReturnPath) {
                case 1:
                    $returnPathEmail = 'support@karlsbach.eu'; // $this->getSenderEmail();
                    break;
                case 2:
                    $returnPathEmail = Mage::getStoreConfig(self::XML_PATH_SENDING_RETURN_PATH_EMAIL);
                    break;
                default:
                    $returnPathEmail = null;
                    break;
            }

            if ($returnPathEmail !== null) {
                $mailTransport = new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
                Zend_Mail::setDefaultTransport($mailTransport);
            }

            $mail->setFrom($sender, 'Karlsbach Shop');
            $mail->setBodyHtml($templateProcessed);
            $mail->addTo($post['email'], '=?utf-8?B?' . base64_encode($post['name']) . '?=');
            $mail->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
            $mail->send();

            $message = $this->__("Newsletter sent to %s. Please check both your Inbox and Spam folders for the Newsletter Test Email.", $post['email']);
            Mage::getSingleton('adminhtml/session')->addSuccess($message);
        }
        catch (Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }
}
