<?php

/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 02.02.16
 * Time         : 0:51
 * Description  :
 */
require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';

class Mage_Checkout_Quick_OrderController extends Mage_Checkout_OnepageController
{
    public function indexAction()
    {
        $this->loadLayout()
            ->getLayout()
            ->getBlock('root')
            ->setTemplate('page/popup.phtml');


        // Create a generic template block
        $block = $this->getLayout()->createBlock('core/template');

        // Assign your template to it
        // This path is relative to your current theme (eg: rwd/default/template/...)
        $block->setTemplate('checkout/quick_order.phtml');

        // Render the template to the browser
        $this->getLayout()->getBlock('content')->append($block);

        $this->renderLayout();
    }

    public function saveAction()
    {
        $this->loadLayout()
            ->getLayout()
            ->getBlock('root')
            ->setTemplate('page/popup.phtml');

        $cart = $this->_getCart();
        $quote = $this->_getQuote();

        if (!$quote->hasItems() || $quote->getHasError()) {
            Mage::getSingleton('checkout/session')->addError($this->__('No items in Cart.'));
            $this->_redirect('*/*/');
            return;
        } else {
            $params = $this->getRequest()->getParams();

            $username = [];
            if (isset($params['billing']) && isset($params['billing']['username']) && !empty($params['billing']['username'])) {
                if (preg_match("/[\s]/i", $params['billing']['username'])) {
                    $username = explode(' ', $params['billing']['username']);
                } else {
                    $username[] = $params['billing']['username'];
                }
            }

            $email = '~~~';
            if (isset($params['billing']['email']) && $params['billing']['email']) {
                $email = $params['billing']['email'];
                $validator = new Zend_Validate_EmailAddress();
                $isEmailValid = $validator->isValid($email);

                if (!$isEmailValid) {
                    Mage::getSingleton('checkout/session')->addError($this->__('Email is not valid.'));
                    $this->_redirect('*/*/');
                    return;
                }
            }

            $telephone = '~~~';
            if (isset($params['billing']['telephone']) && $params['billing']['telephone']) {
                $telephone = $params['billing']['telephone'];
            }

            $company = '~~~';
            if (isset($params['billing']['company']) && $params['billing']['company']) {
                $company = $params['billing']['company'];
            }

            $addressData = array(
                'firstname' => $username[1]?:' ',
                'middlename' => $username[2]?:' ',
                'lastname' => $username[0]?:'[lastname]',
                'street' => ['0'=>'[street]','1'=>'[apt]'],
                'city' => '[city]',
                'postcode' => '[postcode]',
                'telephone' => $telephone,
                'fax' => $telephone,
                'country_id' => 'RU',
                'region_id' => '', // id from directory_country_region table
                'region' => '',
                'use_for_shipping' => 0,
                'customer_email' => $email,
                'email' => $email,
                'company' => $company,
                'vat_id' => '1111111',
            );

            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail(trim($email));
            if ($customer->getId() > 0) {
                $tax_vat = $customer->getTaxvat();
                if (empty($tax_vat)) {
                    $customer->setTaxvat('33333');
                    $customer->save();
                }

                $addressId = $customer->getDefaultBilling();
            } else {
                $customer->setStore(Mage::app()->getStore())
                    ->setFirstname($addressData['firstname'])
                    ->setMiddlename($addressData['middlename'])
                    ->setLastname($addressData['lastname'])
                    ->setEmail($email)
                    ->setTaxvat('777')
                    ->setGroupId(1)
                    ->setPassword($customer->generatePassword());

                try {
                    $customer->save();
                    $quote->setCheckoutMethod('register');
                }
                catch (Exception $e) {
                    Mage::logException($e);
                    Mage::getSingleton('core/session')->addError($e->getMessage());
                }

                /**
                 * Add some address
                 */
                $address = Mage::getModel("customer/address");
                $address->addData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');

                try{
                    $address->save();
                }
                catch (Exception $e) {
                    Mage::logException($e);
                    Mage::getSingleton('core/session')->addError($e->getMessage());
                }

                $addressId = $address->getId();
            }

//            $quote->setCheckoutMethod('guest'); // register | login_in
            $quote->setCustomerIsGuest(false);
            $quote->setCustomerId($customer->getId());
            $quote->setCustomerGroupId(1);
            $quote->assignCustomer($customer);
            $quote->setCustomerFirstname($addressData['firstname']);
            $quote->setCustomerLastname($addressData['lastname']);
            $quote->getBillingAddress()->addData($addressData)->save();
            $quote->getShippingAddress()->addData($addressData);
            $quote->getShippingAddress()->collectShippingRates()->setShippingMethod($params['shipping_method']);
            $quote->getShippingAddress()->setPaymentMethod($params['payment_method']);
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->collectTotals()->save();
            $quote->save();

            $this->_getCart()->setQuote($quote);
            $this->_getSession()->setCustomer($customer);
//            $this->_getCustomerSession()->setCustomer($quote->getCustomer());

            try {
                /* @var $checkout Mage_Checkout_Model_Type_Onepage */
                $onepage = $this->_getOnepage();
                $onepage->setQuote($quote);
                $onepage->initCheckout();
                $onepage->saveCheckoutMethod($quote->getCheckoutMethod());
                $result = $onepage->saveBilling($addressData, $addressId);
                if (!empty($result)) {
                    Mage::log($result);
                }

                $result = $onepage->saveShippingMethod($params['shipping_method']);
                if (!empty($result)) {
                    Mage::log($result);
                }

                $result = $onepage->savePayment(array('method'=>$params['payment_method']));
                if (!empty($result)) {
                    Mage::log($result);
                }

                $onepage->saveOrder();

                $orderId = $this->_getSession()->getLastOrderId();

                if ($orderId > 0) {
                    $cart->truncate()->save();
                    $cart->getItems()->clear()->save();
                    $this->_getCustomerSession()->logout();
                }
            } catch (Exception $ex) {
                Mage::logException($ex);
                Mage::getSingleton('core/session')->addError($ex->getMessage());
                $this->_redirect('*/*/');
            } catch (Mage_Core_Exception $ex) {
                Mage::logException($ex);
                Mage::getSingleton('core/session')->addError($ex->getMessage());
                $this->_redirect('*/*/');
            }

            $block = $this->getLayout()->createBlock('core/template');
            $block->setTemplate('checkout/quick_order_success.phtml');
            $this->getLayout()->getBlock('content')->append($block);
        }

        $this->renderLayout();
    }

    protected function _getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }
}
