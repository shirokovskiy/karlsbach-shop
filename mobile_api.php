<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 11/18/13
 * Time         : 2:55 PM
 * Description  :
 */
//error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
//error_reporting(E_ALL);

require_once "app/Mage.php";
umask(0);
$app = Mage::app('default');

Mage::setIsDeveloperMode(true);
$core_session = Mage::getSingleton('core/session', array('name'=>'frontend'));
$session = Mage::getSingleton('customer/session', array('name'=>'frontend'));
//$helper = Mage::helper('core/translate');
$act = $app->getRequest()->getParam('act');
$locale = $app->getRequest()->getParam('locale'); // возможно больше не существует
$location = $app->getRequest()->getParam('location'); // RU | EU
$store = $app->getRequest()->getParam('store'); // presale | sale

$debug_locale = $locale;

switch ($location) {
    case 'RU':
        $locale = 'ru_RU';
        break;
    default:
        $locale = 'en_US';
        break;
}

$translator = Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);

/**
 * Set default Store View
 */
$store_id = 1;

if ($location == 'EU') {
    if ($store == 'presale') {
        $store_id = 3;
    } else {
        $store_id = 2;
    }
} else {
    if ($store == 'presale') {
        $store_id = 4;
    }
}

$app->setCurrentStore($store_id);

$json['type'] = 'error';
$json['code'] = 1;
$json['errorMsg'] = __('Default Error!');
$json['data'] = null;

$arrErrorMsg[1] = __('Invalid Login or Password!');
$arrErrorMsg[2] = __('Authentication failed!');
$arrErrorMsg[3] = __('Invalid barcode.');
$arrErrorMsg[4] = __('No product by barcode.');
$arrErrorMsg[5] = __('Invalid article number.');
$arrErrorMsg[6] = __('No data by article number.');
$arrErrorMsg[7] = __('Invalid data query. Please, authorize.');
$arrErrorMsg[8] = __('Something goes wrong! Probably auth time has out.');
$arrErrorMsg[9] = __('Unknown product ID.');
$arrErrorMsg[10] = __('Can not add items to shopping cart.');
$arrErrorMsg[11] = __('Discounts error.');

Mage::log($app->getRequest()->getParams());
Mage::log('store_id = '.$store_id);

$SSID = $SID = null;

switch ($act) {
    case 'auth':
        $login = $app->getRequest()->getPost('login');
        $password = $app->getRequest()->getPost('password');
        $_is_auth = false;

        try {
            if ($session->login ( $login, $password )) {
                $_is_auth = true;
            }
        } catch(Exception $ex) {
            $json['errorMsg'] =  $arrErrorMsg[$json['code']];

            if (isset($_GET['debug']) && $_GET['debug']==1):
                ?>Line: <?php echo __LINE__ ?>. <br>
                <div>errorMsg: <?php echo $json['errorMsg'] ?></div>
                <div><?php echo $ex->getMessage() ?></div>
                <?php exit();
            endif;
        }

        try {
            if ($_is_auth) {
                $json['type'] = 'ok';
                $json['code'] = 0;
                $json['errorMsg'] = __('Welcome to Karlsbach App.');
                $json['data'] = new stdClass();
                $json['data']->sid = $session->getSessionId();
//                $json['data']->id = $session->getCustomerId();
                $json['data']->login = $login;
                $session->setCustomerAsLoggedIn($session->getCustomer());
                $SSID = md5(md5($json['data']->sid).$login);

                Mage::log($json);

                if (isset($_GET['debug']) && $_GET['debug']==1):
?>
                    <div>
                        <h2><?php echo __('Are you sure?'); ?></h2>
                        <?php echo $debug_locale;
                        try { ?>
                            <form action="/mobile_api.php?act=checkCart&debug=1&locale=<?php echo $debug_locale ?>" method="post">
                                <input type="hidden" id="login" name="login" value="<?php echo $login ?>" class="plane" maxlength="255"/>
                                <input type="hidden" id="SID" name="SID" value="<?php echo $json['data']->sid ?>" class="plane" maxlength="255"/>
                                <div>SSID:
                                    <input type="text" id="sid" name="SSID" value="<?php echo $SSID ?>" class="plane" maxlength="255"/>
                                </div>
                                <p>SID: <?php echo $json['data']->sid ?></p>
                                <div>
                                    <input type="submit" value="Check auth"/>
                                </div>
                            </form>
                        <?php } catch(Exception $ex) {echo $ex->getTraceAsString();} ?>
                    </div>
<?php               exit();
                endif;
            }
        } catch (Exception $ex) {
            $json['code'] = 13;
            $json['errorMsg'] = $ex->getMessage();
            if (isset($_GET['debug']) && $_GET['debug']==1):
                ?>Line: <?php echo __LINE__ ?>. <br>
                <div><?php echo $ex->getMessage();?></div>
                <?php exit();
            endif;
        }

        break;

    case 'logout':
        break;

    case 'getProduct': // http://shop.karlsbach.eu/mobile_api.php?act=getProduct&barcode=4741513000014&store=sale&location=RU
        $barcode = $app->getRequest()->getParam('barcode');
        if (!empty($barcode)) {
            $like_sku = 'erp_1C_s';
            switch ($store_id) {
                case 2:
                case 3:
                    $like_sku = 'directo_s';
                    break;
                default:
                    break;
            }

            $_Product = Mage::getModel('catalog/product')
                ->setStoreId($store_id)
                ->getResourceCollection()
                ->addAttributeToSelect('*')
//                ->joinField('qty',                     // todo: продукт также должен быть на складе
//                    'cataloginventory/stock_item',
//                    'qty',
//                    'product_id=entity_id',
//                    '{{table}}.stock_id=1',
//                    'left')
//                ->addAttributeToFilter('qty', array("gt" => 0))
                ->addStoreFilter($store_id)
                ->addAttributeToFilter('barcode', $barcode)
                ->addAttributeToFilter('sku', array('like' => $like_sku.'%'))
                ->addAttributeToFilter(
                    'status',
                    array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                )
                ->setPage(1,1)
                ->getFirstItem();


            if ($_Product !== false && $_Product->getId()) {
                $_Product = $_Product->load($_Product->getId());
                $pack_qty = $_Product->getData('pack_qty');
                $start_order_qty = $_Product->getData('start_order_qty');
                $max_qty = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($_Product)->getQty();

                if (in_array($store_id, array(1,2)) && $max_qty <= 0) {
                    Mage::log('$max_qty = '.$max_qty);

                    // Error
                    $json['code'] = 6; // No Product Data
                    $json['errorMsg'] = $arrErrorMsg[$json['code']];

                } else {
                    $_imageUrl = Mage::helper('catalog/image')->init($_Product, 'small_image')->resize(100,100);
                    $_imagePath = str_replace("http://".$_SERVER['SERVER_NAME']."/", BP.DS, $_imageUrl);
                    $_imageBase64 = '';
                    if (file_exists($_imagePath)) {
                        $_imageType = pathinfo($_imagePath, PATHINFO_EXTENSION);
                        $data = file_get_contents($_imagePath);
                        $_imageBase64 = 'data:image/' . $_imageType . ';base64,' . base64_encode($data);
                    }

                    $json['type'] = 'ok';
                    $json['code'] = 0;
                    $json['errorMsg'] = __('Product details.');
                    $json['data'] = new stdClass();
                    $json['data']->productId = $_Product->getId();
                    $json['data']->productName = htmlentities($_Product->getName());
                    $json['data']->productArticle = htmlentities($_Product->getArticle());
                    $json['data']->thumbnail = $_imageBase64;
                    $json['data']->productNote = $pack_qty > 1 ? __("%s per pack.", $pack_qty) : '';
                    $json['data']->productPackQty = $pack_qty > 1 ? $pack_qty : 1;
                    $json['data']->productMinOrderQty = $start_order_qty > $json['data']->productPackQty ? $start_order_qty : $json['data']->productPackQty;
                    $json['data']->productMaxQty = $max_qty > $json['data']->productPackQty ? $max_qty : $json['data']->productPackQty;
                    $json['data']->productPrice = $_Product->getPrice();
                }

                if (isset($_GET['debug']) && $_GET['debug']==1):
                    ?>
                    <div><?php Zend_Debug::dump($json) ?></div>
                    <?php exit();
                endif;
            } else {
                // Error
                $json['code'] = 4; // No Product Data
                $json['errorMsg'] = $arrErrorMsg[$json['code']];
            }
        } else {
            // Error
            $json['code'] = 3; // No Barcode
            $json['errorMsg'] = $arrErrorMsg[$json['code']];
        }
        break;

    case 'getProductByArticle': // http://shop.karlsbach.eu/mobile_api.php?act=getProductByArticle&article=00616&store=presale&location=EU
        $article = $app->getRequest()->getParam('article');
        if (!empty($article)) {
            if ($location == 'RU') {
                $article = 'В'.$article;  // добавляем русскую В
//                $_Product = Mage::getModel('catalog/product')->setStoreId($store_id)->loadByAttribute('article', $article);
                $like_sku = 'erp_1C_s';
                switch ($store_id) {
                    case 2:
                    case 3:
                        $like_sku = 'directo_s';
                        break;
                    default:
                        break;
                }

                $_Product = Mage::getModel('catalog/product')->setStoreId($store_id)->getResourceCollection()
                    ->addAttributeToSelect('*')
                    ->addStoreFilter($store_id)
                    ->addAttributeToFilter('article', $article)
                    ->addAttributeToFilter('sku', array('like' => $like_sku.'%'))
                    ->addAttributeToFilter(
                        'status',
                        array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                    )
                    ->setPage(1,1)
//                    ->getSelectSql()->__toString();
                    ->getFirstItem();//->getId();//->getData();

            } elseif ($location == 'EU') {
                $_Product = Mage::getModel('catalog/product')->setStoreId($store_id)->loadByAttribute('sku', 'directo_s'.$store_id.'_'.$article);
            }

            if ($_Product !== false) {
                $_Product = $_Product->load($_Product->getId());

                $pack_qty = $_Product->getData('pack_qty');
                $start_order_qty = $_Product->getData('start_order_qty');
                $max_qty = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($_Product)->getQty();

                if (in_array($store_id, array(1,2)) && $max_qty <= 0) {
                    Mage::log('$max_qty = '.$max_qty);

                    // Error
                    $json['code'] = 6; // No Product Data
                    $json['errorMsg'] = $arrErrorMsg[$json['code']];

                } else {
                    $_imageUrl = Mage::helper('catalog/image')->init($_Product, 'small_image')->resize(100,100);
                    $_imagePath = str_replace("http://".$_SERVER['SERVER_NAME']."/", BP.DS, $_imageUrl);
                    $_imageBase64 = '';
                    if (file_exists($_imagePath)) {
                        $_imageType = pathinfo($_imagePath, PATHINFO_EXTENSION);
                        $data = file_get_contents($_imagePath);
                        $_imageBase64 = 'data:image/' . $_imageType . ';base64,' . base64_encode($data);
                    }

                    $json['type'] = 'ok';
                    $json['code'] = 0;
                    $json['errorMsg'] = __('Product details.');
                    $json['data'] = new stdClass();
                    $json['data']->productId = $_Product->getId();
                    $json['data']->productName = htmlentities($_Product->getName());
                    $json['data']->productArticle = htmlentities($_Product->getArticle());
                    $json['data']->thumbnail = $_imageBase64;
                    $json['data']->productNote = $pack_qty > 1 ? __("%s per pack.", $pack_qty) : '';
                    $json['data']->productPackQty = $pack_qty > 1 ? $pack_qty : 1;
                    $json['data']->productMinOrderQty = $start_order_qty > $json['data']->productPackQty ? $start_order_qty : $json['data']->productPackQty;
                    $json['data']->productMaxQty = $max_qty > $json['data']->productPackQty ? $max_qty : $json['data']->productPackQty;
                    $json['data']->productPrice = $_Product->getPrice();
                }

                if (isset($_GET['debug']) && $_GET['debug']==1):
                    ?>
                    <div><?php Zend_Debug::dump($json) ?></div>
                    <?php exit();
                endif;
            } else {
                // Error
                $json['code'] = 6; // No Product Data
                $json['errorMsg'] = $arrErrorMsg[$json['code']];
            }
        } else {
            // Error
            $json['code'] = 5; // No Barcode
            $json['errorMsg'] = $arrErrorMsg[$json['code']];
        }
        break;

    case 'checkCart':
        $SSID = $app->getRequest()->getPost('SSID');
        $login = $app->getRequest()->getPost('login');
        $SID = $app->getRequest()->getPost('SID');

        if (strlen($SSID)==32 && !empty($SID)) {
            $currentSessionId = $session->getSessionId();
            $encryptedSessionId = md5(md5($currentSessionId).$login);

            if ($encryptedSessionId == $SSID) {
                if ( $session->isLoggedin() ) {
                    $fn = $session->getCustomer()->getData('firstname');
                    $ln = $session->getCustomer()->getData('lastname');
                    if (isset($_GET['debug']) && $_GET['debug']==1):
                        ?>
                        <div><?php echo __('Welcome! '.$fn.' '.$ln.'!'); ?></div>
                        <div><form action="/mobile_api.php?act=neworder&prd[8118]=100&prd[8117]=20&prd[8116]=5&debug=1&locale=<?php echo $debug_locale ?>" method="post">
                            <input type="hidden" name="login" value="<?php echo $login ?>" maxlength="255"/>
                            <input type="hidden" name="SSID" value="<?php echo $SSID ?>" maxlength="255"/>
                            <input type="hidden" name="SID" value="<?php echo $SID ?>" maxlength="255"/>
                            <div>
                                <input type="submit" value="Order products"/>
                            </div>
                        </form></div>

                        <div><form action="/mobile_api.php?act=getProductByArticle&debug=1&locale=<?php echo $debug_locale ?>" method="post">
                                <input type="text" name="article" value="" maxlength="255"/>
                                <input type="hidden" name="store" value="sale"/>
                                <input type="hidden" name="location" value="RU"/>
                                <div>
                                    <input type="submit" value="Check product by Article"/>
                                </div>
                            </form></div>

                        <div><form action="/mobile_api.php?act=getProduct&debug=1&locale=<?php echo $debug_locale ?>" method="post">
                                <input type="text" name="barcode" value="" maxlength="255"/>
                                <input type="hidden" name="store" value="sale"/>
                                <input type="hidden" name="location" value="RU"/>
                                <div>
                                    <input type="submit" value="Check product by Barcode"/>
                                </div>
                            </form></div>
                        <?php exit();
                    endif;

                    $json['type'] = 'ok';
                    $json['code'] = 0;
                    $json['errorMsg'] = __('Welcome! '.$fn.' '.$ln.'!');
                } else {
                    $json['code'] = 2;
                    $json['errorMsg'] = $arrErrorMsg[$json['code']];
                }
            } else {
                $json['code'] = 8; // Auth Error
                $json['errorMsg'] = $arrErrorMsg[$json['code']];
            }
        } else {
            $json['code'] = 7; // Auth Error
            $json['errorMsg'] = $arrErrorMsg[$json['code']];
            if (isset($_GET['debug']) && $_GET['debug']==1):
                ?>
                <div><?php echo $ex->getMessage();?></div>
                <?php           exit();
            endif;
        }
        break;

    case 'neworder':
        $SSID = $app->getRequest()->getPost('SSID');
        $login = $app->getRequest()->getPost('login');
        $SID = $app->getRequest()->getPost('SID');

        Mage::log('neworder $login = '.$login);
        Mage::log('neworder $SSID = '.$SSID);
        Mage::log('neworder $SID = '.$SID);

        $json['code'] = 0;
        $json['errorMsg'] = __('Order sent.');

        if (strlen($SSID)==32 && !empty($SID)) {
            $currentSessionId = $session->getSessionId();
            Mage::log('neworder $currentSessionId = '.$currentSessionId);
            $encryptedSessionId = md5(md5($currentSessionId).$login);

            Mage::log('SIDs passed but not checked yet. Lets check it now!');

            $email = $login;
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId($app->getWebsite()->getId());
            $customer->loadByEmail(trim($email));
            $session->loginById($customer->getId());

//            if ($encryptedSessionId == $SSID) {
                if ( $session->isLoggedin() ) {
                    // С авторизацией всё в порядке! Теперь попробуем разместить заказ.
                    $prd = $app->getRequest()->getParam('prd');

                    if (is_array($prd) && !empty($prd)) {
                        // Если есть данные по продуктам, запустим корзину и сложим в неё продукты
                        $customer_id = $session->getCustomerId();
//                        $_Cart = Mage::getModel('sales/quote')->loadByCustomer($customer_id);
                        $_Cart = Mage::getModel('checkout/cart');
                        $_Cart->init();

                        $remindProducts = array();

                        foreach ($prd as $productId => $prd_qty) {
                            // Загрузить продукт
                            if (intval($productId)) {
                                $_Product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($productId);

                                /*if (!$_Product->isSalable() && $store_id==1 && $location == 'RU') {
                                    $store_id = 4;
                                    $_Product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($productId);
                                    Mage::log('Reset Store ID to:'.$store_id.' and Product now is:');
//                                    Mage::log($_Product);
                                }*/

                                $remindProducts[$productId]['name'] = $_Product->getName();
                                $remindProducts[$productId]['qty'] = $prd_qty;
                                $remindProducts[$productId]['link'] = $_Product->getProductUrl();
                                $remindProducts[$productId]['img'] = $_Product->getImageUrl();
                                $remindProducts[$productId]['article'] = $_Product->getArticle();

                                if ($_Product->getId() && $_Product->isSalable() && $_Product->isInStock()) {
                                    // Если такой продукт существует и продаваемый, то добавим его
                                    $_Cart->addProduct($_Product, array('qty' => $prd_qty));
                                } else {
                                    // Нет такого продукта
                                    Mage::log("Unknown Product by Id! ".$productId);
//                                    Mage::log($_Product->getData());
                                }
                            } else {
                                Mage::log("Unknown productId! ".$productId);

                                $json['code'] = 9; // No product data
                                $json['errorMsg'] = $arrErrorMsg[$json['code']];
                                break;
                            }
                        }

                        if ($json['code'] == 0) {
                            try {
                                /*$_Cart->addProductsByIds(array( $product_id ));*/
                                $_Cart->save();
                                Mage::log('Save Cart');
                            } catch (Exception $ex) {
                                $json['code'] = 10; // Cant save Cart
                                $json['errorMsg'] = $arrErrorMsg[$json['code']];

                                if (isset($_GET['debug']) && $_GET['debug']==1):
                                    ?>
                                    <div><?php echo $ex->getTraceAsString();?></div>
                                    <?php           exit();
                                endif;
                            }

                            if ($json['code'] == 0) {
                                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
                                Mage::log('Cart was updated');

                                //if ($customer_id == 1) {
                                    // Send Email
                                    try {
                                        // Transactional Email Template's ID
                                        if ($location == 'EU') {
                                            $templateId = 1;
                                        } else {
                                            $templateId = 3;
                                        }

                                        // Set sender information
                                        $senderName = Mage::getStoreConfig('trans_email/ident_support/name');
                                        $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
                                        $sender = array('name' => $senderName,
                                            'email' => $senderEmail);

                                        $customerData = Mage::getModel('customer/customer')->load($customer_id)->getData();
                                        // Set recipient information
                                        $recipientEmail = $customerData['email'];
                                        $recipientName = $customerData['firstname'].' '.$customerData['lastname'];

                                        // Set variables that can be used in email template
                                        $emailTemplateVariables = array('customerName' => $recipientName
                                            ,'customerEmail' => $recipientEmail
                                            ,'cartProducts' => 'x-x-x-x'
                                        );

                                        if (!empty($remindProducts)) {
                                            $emailTemplateVariables['cartProducts'] = '<table><tr><th>'.__('Image').'</th><th>'.__('Title').'</th><th width="90">'.__('Article').'</th><th width="60">'.__('Qty').'</th></tr>';
                                            foreach ($remindProducts as $pID => $arrPrd) {
                                                $emailTemplateVariables['cartProducts'] .= '<tr><td><img src="'.$arrPrd['img'].'" style="width:75px; height: 75px" /></td><td><a href="'.$arrPrd['link'].'">'.$arrPrd['name'].'</a></td><td align="center">'.$arrPrd['article'].'</td><td align="center">'.$arrPrd['qty'].'</td></tr>';
                                            }
                                            $emailTemplateVariables['cartProducts'] .= '</table>';
                                        }

                                        // Send Transactional Email
                                        $emailTemplate = Mage::getModel('core/email_template');

                                        if ($customer_id != 1) {
                                            $emailTemplate->addBcc('jaan@karlsbach.eu')
                                                ->addBcc('karlsbach@karlsbach.eu')
                                                ->addBcc('jimmy.webstudio@gmail.com');
                                        }

                                        $emailTemplate->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $emailTemplateVariables, $store_id);

                                        $translator->setTranslateInline(true);
                                    } catch (Exception $ex) {
                                        Mage::log( $ex->getTraceAsString() );
                                    }
                                //}
                            }
                        }
                    }

                    if (isset($_GET['debug']) && $_GET['debug']==1):
                        ?>
                        <div><?php Zend_Debug::dump($prd) ?></div>
                        <?php           exit();
                    endif;

                    if ($json['code'] == 0) {
                        $json['type'] = 'ok';
                    }
                } else {
                    $json['code'] = 2;
                    $json['errorMsg'] = $arrErrorMsg[$json['code']];
                    Mage::log('Authentication failed! Current session ID:'.$currentSessionId);
                }
//            } else {
//                $json['code'] = 8; // Auth Error
//                $json['errorMsg'] = $arrErrorMsg[$json['code']];
//                Mage::log('Encrypted sessions not equal: '.$encryptedSessionId .'!='. $SSID);
//            }
        } else {
            $json['code'] = 7; // Auth Error
            $json['errorMsg'] = $arrErrorMsg[$json['code']];
            Mage::log('Invalid data query. Please, authorize.');
            if (isset($_GET['debug']) && $_GET['debug']==1):
                ?>
                <div><?php echo $ex->getMessage();?></div>
                <?php           exit();
            endif;
        }
        break;

    case 'discounts':
        // discounts : { sale: [{price: 2500, discount: 0.05}, {price: 5000, discount: 0.1}],
        //            presale: [{price: 3000, discount: 0.09}, {price: 6000, discount: 0.14}] }
        if (in_array($location, array('RU', 'EU')) && in_array($store, array('sale', 'presale'))) {
            $json['type'] = 'ok';
            $json['code'] = 0;
            $json['errorMsg'] = __('Discounts.');

            $d = new stdClass();
            $d->price = 2500;
            $d->discount = 0.3;
            $discount['EU']['sale'][] = $d;

            $d = new stdClass();
            $d->price = 7000;
            $d->discount = 0.3;
            $discount['EU']['sale'][] = $d;

            $d = new stdClass();
            $d->price = 25000;
            $d->discount = 0.3;
            $discount['EU']['sale'][] = $d;

            //*****

            $d = new stdClass();
            $d->price = 1;
            $d->discount = 0;
            $discount['EU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 2000;
            $d->discount = 0.2;
            $discount['EU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 4667;
            $d->discount = 0.3;
            $discount['EU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 10667;
            $d->discount = 0.35;
            $discount['EU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 17334;
            $d->discount = 0.4;
            $discount['EU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 25321;
            $d->discount = 0.45;
            $discount['EU']['presale'][] = $d;

            //*******

            $d = new stdClass();
            $d->price = 200000;
            $d->discount = 0.05;
            $discount['RU']['sale'][] = $d;

            $d = new stdClass();
            $d->price = 400000;
            $d->discount = 0.1;
            $discount['RU']['sale'][] = $d;

            $d = new stdClass();
            $d->price = 10000000;
            $d->discount = 0.2;
            $discount['RU']['sale'][] = $d;

            //*******

            $d = new stdClass();
            $d->price = 1;
            $d->discount = 0;
            $discount['RU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 1923;
            $d->discount = 0.2;
            $discount['RU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 4487;
            $d->discount = 0.3;
            $discount['RU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 10256;
            $d->discount = 0.35;
            $discount['RU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 16666;
            $d->discount = 0.4;
            $discount['RU']['presale'][] = $d;

            $d = new stdClass();
            $d->price = 24358;
            $d->discount = 0.45;
            $discount['RU']['presale'][] = $d;

            $discounts = $discount[$location][$store];

            $json['data']['discounts'] = $discounts;
        } else {
            $json['code'] = 11;
            $json['errorMsg'] = $arrErrorMsg[$json['code']];
        }

        break;

    default:
        break;
}

Mage::log($json);
header('Content-type: application/json; charset=utf-8');
echo json_encode($json);

//
//
//
