Clone this magmi folder to another project.
Rename folder _PROJECT_PATH_/magmi/conf/McBuyUpd/ with new name, i.e. YouPrjUpd.
Fix path in _PROJECT_PATH_/magmi/conf/Magmi_CSVDataSource.conf & /magmi/conf/YouPrjUpd/Magmi_CSVDataSource.conf
Fix credentials and path in _PROJECT_PATH_/magmi/conf/magmi.ini
Fix _PROJECT_PATH_/magmi/.htaccess
Copy files _PROJECT_PATH_/shell/*.sql into projects parent of root dir, i.e. /var/www/data/project/magmi_shells/, and fix them.
Create folders _PROJECT_PATH_/var/import & _PROJECT_PATH_/var/import/media & _PROJECT_PATH_/media/import (last one for native magmi settings)
Set "mount --bind _PROJECT_PATH_/media/import/ _PROJECT_PATH_/var/import/media/"
Enjoy! 