/**
 * Created by Dmitry Sirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 9/12/13
 * Time: 3:48 PM
 * Description:
 */
if (jQuery) {
    var $window    = jQuery(window);
    jQuery(function() {
        topPadding = 5;
        $sidebar   = jQuery(".custom-warenkorb");
        if ($sidebar.length > 0) {
            var offset = $sidebar.offset();
            var defaultMarginTop = $sidebar.css('marginTop');

            $window.scroll(function(){
                if ($window.scrollTop() > offset.top) {
                    if (!$sidebar.hasClass('bordered')) {
                        $sidebar.addClass('bordered');
                    }

                    $sidebar.stop().animate({
                        marginTop: $window.scrollTop() - offset.top + topPadding
                    });
                } else {
                    $sidebar.removeClass('bordered');
                    $sidebar.stop().animate({
                        marginTop: defaultMarginTop
                    });
                }
            });
        }
    });
}
