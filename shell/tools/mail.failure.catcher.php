#!/usr/bin/env php
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 18.06.15
 * Time         : 19:50
 * Description  : This script catches Mail Failure reports from given mail-box
 */


/**
 * Инициализация
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

/**
 * Check if process running
 */
$cli = "ps aux | grep -i '".basename(__FILE__)."' | grep -v grep";
exec($cli, $pids);
if (is_array($pids) && !empty($pids)) {
    echo "Process is running already.".PHP_EOL;
    print_r($pids);

    $pids = array_filter($pids, function($value){
        return !preg_match("/\.log/i", $value);
    });

    echo "After filtering processes.".PHP_EOL;
    print_r($pids);

    if (count($pids) > 1) {
        echo "Filtered processes:".PHP_EOL;
        print_r($pids);
        echo "Stop execution current.".PHP_EOL;
        die();
    }
}

$DR = dirname(__FILE__);
$filename = $DR.'/z.mail.failure.catcher.'.date("Ymd").'.log';
$regexp = '/[[:alnum:]][\w\d\.\-_]{0,100}@[\w\d\.\-_]+\.[[:alpha:]]{2,6}/i'; // регулярка на Email

/**
 * Параметры
 */
$qtyMessages = 100;
if (isset($argv[1]) && !empty($argv[1])) {
    $qtyMessages = (int)$argv[1];
}

/**
 * Подключение базы данных
 * Там где лежит база плохих и хороших ящиков
 */
try {
    $dbh = new PDO('mysql:host=localhost;dbname=test', 'root', '1234qaz');
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    print "PDO Error: " . $e->getMessage() . "\n";
    die("\n");
}

$mail = new Email_reader();

/**
 * Критерии поиска в ящике сообщений
 */
$criteria = 'ALL';
$criteria = 'ALL TO "support@karlsbach.eu"';
//$criteria = 'FROM "Mail Delivery Subsystem"';
//$criteria = 'FROM "Mailer-Deamon"';
//$criteria = 'FROM "postmaster"';
//$criteria = 'FROM "Mail Delivery" TO "support@karlsbach.eu"';

$list = $mail->inbox($criteria, $qtyMessages);

file_put_contents($filename, 'All messages:'.$mail->msg_cnt.', Found:'.$mail->msg_cnt_unread."\n", FILE_APPEND);
//$list = null;
if (is_array($list) && !empty($list)) {
    foreach ($list as $message) {
        //file_put_contents($filename, "FROM ".$message['overview']->from."\n", FILE_APPEND);
        $ms = null;
        $subject = $message['overview']->subject;
        if (preg_match("/\?/",$subject, $ms) && count($ms) > 1) {
            $subject = $mail->get_header($subject);
        }

        # Проверим от кого это письмо
        if (preg_match('/MAILER\-DAEMON/i', $message['overview']->from)
            || preg_match('/postmaster/i', $message['overview']->from)
            || preg_match('/spam/i', $message['overview']->from)
            || preg_match('/Mail Delivery Subsystem/i', $message['overview']->from)
            || preg_match('/MDaemon/i', $message['overview']->from)
            || preg_match('/maildaemon/i', $message['overview']->from)
            || preg_match('/antispam/i', $message['overview']->from)
            || (preg_match('/Undeliver/i', $subject)
                || preg_match('/Не удается доставить/i', $subject)
                || preg_match('/Возвращенное сообщение/i', $subject)
                || preg_match('/Delivery status notification/i', $subject)
                || preg_match('/FAILURE/i', $subject)
                || preg_match('/rejected/i', $subject)
                || preg_match('/Returned/i', $subject)
                || preg_match('/SPAM/i', $subject)
                || preg_match('/Spam\]/i', $subject)
                || preg_match('/Mail delivery failed/i', $subject)
                || preg_match('/No such user/i', $subject)
                || preg_match('/Automatically rejected mail/i', $subject)
                || preg_match('/Automatic reply/i', $subject)
                || preg_match('/Automatic Response/i', $subject)
                || preg_match('/Autoreply\:/i', $subject)
                || preg_match('/Автоматический ответ/i', $subject)
                || preg_match('/User unknown/i', $subject))
            || (preg_match('/550 5\.1\.1/i', $message['body'])
                || preg_match('/Mailbox.for.user.is.full/i', $message['body'])
                || preg_match('/550 4\.4\.7/i', $message['body'])
                || preg_match('/automatic response/i', $message['body'])
                || preg_match('/Доставка следующим получателям или группам отложена/i', $message['body'])
                || preg_match('/Не удалось выполнить доставку/i', $message['body'])
                || preg_match('/Mailbox.invalid/i', $message['body']))
        ) {
            $matches = array();

            if (false === preg_match_all($regexp, $message['body'], $matches)) {
                file_put_contents($filename, "No eMails found in message ".$message['overview']->message_id.', date:'.$message['overview']->date."\n", FILE_APPEND);
                continue;
            }
            if (!isset($matches[0][0])) {
                preg_match_all($regexp, $subject, $matches);
            }
            if (!isset($matches[0][0])) {
                file_put_contents($filename, "No data in message ".$message['overview']->message_id.', date:'.$message['overview']->date."\n".'Subject:'.$subject."\n".'From:'.$message['overview']->from."\n".'Body:'.print_r($message,true)."\n", FILE_APPEND);
                continue;
            } else $count_emails = count($matches[0]);

            #file_put_contents($filename, "After preg_match found emails: ".$count_emails."\n", FILE_APPEND);
            #file_put_contents($filename, "Found message: ".$message['overview']->message_id.', date:'.$message['overview']->date."\n".'Subject:'.$subject."\n".'From:'.$message['overview']->from."\n", FILE_APPEND);

            // Уберём дубляжи
            $emails = array_unique($matches[0]);

            $sql = "REPLACE INTO emails_private (`email`,`type`) values (:email, 'ignored')";

            $isCatched = false;

            foreach($emails as $email) {
                try {
                    $handle = $dbh->prepare($sql);
                    $handle->bindValue(':email', $email);
                    if ($handle->execute()) {
                        $isCatched = true;
                    } else {
                        file_put_contents($filename, "Cannot run SQL: ".$handle->queryString."\n", FILE_APPEND);
                    }
                } catch (PDOException $e) {
                    $errorInfo = $handle->errorInfo();

                    if ($errorInfo[1] == 1062) {
                        // inform user, throw a different exception, etc
                        file_put_contents($filename, "Duplicate email: ".$email."\n", FILE_APPEND);
                        $isCatched = true;
                    } else {
                        print "PDO Error: " . $e->getMessage() . "\n";
                        die("\n");
                    }
                }
            }

            # Установим письмо как прочтённое
            //$mail->setAsRead($message['overview']->msgno);

            if ($isCatched) {
                # Пометить для удаления
                $mail->mark2delete($message['overview']->msgno);
                file_put_contents($filename, "Mark to delete #".$message['overview']->msgno."\n", FILE_APPEND);
            }
        } else {
            file_put_contents($filename, 'No regexp matched ~ Mail from: '.$message['overview']->from."\n".'~ Subject: '.$subject."\n", FILE_APPEND);
        }
    }

    $mail->expunge();
}

$mail->close(true);

file_put_contents($filename, 'The End at '.date('H:i:s')."!\n\n * * *\n\n", FILE_APPEND);







include "cls.mail.php";
$cMail = new clsMail('Sandbox-'.date('YmdHi'));
$kBytes = 5;
$sql = "REPLACE INTO emails_private (`email`,`type`) values (:email, 'ignored')";
$file_name = '/var/lib/sendmail/dead.letter';
$source_file = fopen( $file_name, "r" ) or die("Couldn't open $file_name");
if ($source_file && 1==2)
    while (!feof($source_file)) {
        $line = fread($source_file, (1024 * $kBytes));
        if ($line === false) {
            throw new Exception("File $file_name read error");
        }
        if (!empty($previous_line_part))
            $line = $previous_line_part.$line;
        $previous_line_part = substr($line,-24);

        ++$gc;
        $matches = array();

        if (false === preg_match_all('/[[:alnum:]][\w\.-_]{0,100}@[\w\.-]+\.[[:alpha:]]{2,6}/i', $line, $matches)) {
            echo "$gc - Line export failed\n";
            continue;
        }
        if (!isset($matches[0][0])) {
            continue;
        } else $count_emails = count($matches[0]);

        // Уберём дубляжи
        $emails = array_unique($matches[0]);

        foreach($emails as $email) {
            $email = str_replace("From:<","",$email);
            $email = str_replace("To:<","",$email);
            $email = str_replace("<br>","",$email);
            $email = str_replace(">","",$email);
            $email = str_replace("rcpt=<","",$email);
            if ($cMail->validate_mail($email)) {
                try {
                    $handle = $dbh->prepare($sql);
                    $handle->bindValue(':email', $email);
                    if ($handle->execute()) {
                        $isCatched = true;
                    } else {
                        file_put_contents($filename, "Cannot run SQL: ".$handle->queryString."\n", FILE_APPEND);
                    }
                } catch (PDOException $e) {
                    $errorInfo = $handle->errorInfo();

                    if ($errorInfo[1] == 1062) {
                        // inform user, throw a different exception, etc
                        file_put_contents($filename, "Duplicate email: ".$email."\n", FILE_APPEND);
                        $isCatched = true;
                    } else {
                        print "PDO Error: " . $e->getMessage() . "\n";
                        die("\n");
                    }
                }
            } else {
                echo "Плохой:".$email,"\n";
            }
        }
    }


/**
 * Class Email_reader
 */
class Email_reader {
    // imap server connection
    public $conn;

    // inbox storage and inbox message count
    public $inbox;
    private $folders;
    public $msg_cnt;
    public $msg_cnt_unread;

    // email login credentials
    private $server = 'mail.zone.ee';
    private $user   = 'support@karlsbach.eu'; // к какому ящику логинимся чтобы проверять по критериям
    private $pass   = '1q2w3e4Rd';
    private $port   = 143; // adjust according to server settings

    // connect to the server and get the inbox emails
    public function __construct() {
        $this->connect();
    }

    // close the server connection
    function close($rm = false) {
        $this->inbox = array();
        $this->msg_cnt = 0;

        if ($rm) $this->expunge();
        imap_close($this->conn);
    }

    // delete marked messages
    public function expunge() {
        imap_expunge($this->conn);
    }

    // open the server connection
    // the imap_open function parameters will need to be changed for the particular server
    // these are laid out to connect to a Dreamhost IMAP server
    private function connect() {
        $this->conn = imap_open('{'.$this->server.':'. $this->port.'/notls}', $this->user, $this->pass);
    }

    // move the message to a new folder
    public function move($msg_index, $folder='INBOX.Processed') {
        // move on server
        imap_mail_move($this->conn, $msg_index, $folder);
        imap_expunge($this->conn);

        // re-read the inbox
        $this->inbox();
    }

    public function folders() {
        $this->folders = imap_list($this->conn, '{'.$this->server.':'. $this->port.'/notls}', "*");

        if ($this->folders == false) {
            echo "imap_list failed: " . imap_last_error() . "\n";
            return false;
        }

        return $this->folders;
    }

    // get a specific message (1 = first email, 2 = second email, etc.)
    public function get($msg_index=NULL) {
        if (count($this->inbox) <= 0) {
            return array();
        }
        elseif ( ! is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }

        return $this->inbox[0];
    }

    // read the inbox
    public function inbox($criteria = 'UNSEEN', $n = 100) {
        global $filename;
        $this->msg_cnt = imap_num_msg($this->conn);

        file_put_contents($filename, 'Criteria to search:'.$criteria."\n", FILE_APPEND);

        $messages = imap_search($this->conn, $criteria);
        /* put the newest emails on top */
        if (is_array($messages) && !empty($messages)) {
            rsort($messages);
        } else {
            file_put_contents($filename, 'No found messages:'.var_export($messages)."\n", FILE_APPEND);
            return false;
        }

        $this->msg_cnt_unread = count($messages);

        $in = array();
        $i = 0;
        foreach($messages as $messageID) {
            /* get information specific to this email */
            $overview = imap_fetch_overview($this->conn,$messageID,0);
            $html = imap_fetchbody($this->conn,$messageID,2); // 2) - HTML
            $text = imap_fetchbody($this->conn,$messageID,1); // 1) - PLAIN

            $in[] = array(
                'index'         => $messageID
                , 'overview'    => $overview[0]
                , 'body'        => (!empty($html) ? $html : $text)
            );

            if ($i > $n) break;
            $i++;
        }

        $this->inbox = $in;
        return $in;
    }

    public function setAsRead($msgNo) {
        if (intval($msgNo)<=0) return false;
        return imap_setflag_full($this->conn, $msgNo, '\\Seen'); #  \\Flagged - звёздочка
    }

    public function mark2delete($msgNo) {
        if (intval($msgNo)<=0) return false;
        imap_delete($this->conn, $msgNo);
    }

    public function get_header($subject)
    {
        if ($subject != null)
        {
            $arr = explode('?', $subject, 4);
            if ($arr != null)
            {
                if (count($arr) >= 4)
                {
                    $in_charset = $arr[1];
                    $text = $arr[3];
                    if ($text != null)
                    {
                        if (strlen($text) >= 3 && $arr[2] != null)
                        {
                            $text = substr($text, 0, strlen($text)-2);
                            $text = $arr[2] == 'Q' ? quoted_printable_decode($text) : base64_decode($text);
                            return !preg_match('/utf\-8/i', $in_charset) ? iconv($in_charset, 'UTF-8', $text) : $text;
                        }
                    }
                }
            }
        }
        else return null;
    }
}


/**
 * Array
(
    [index] => 20702
    [overview] => stdClass Object
    (
        [subject] => Returned mail: see transcript for details
        [from] => Mail Delivery Subsystem <MAILER-DAEMON@karlsbach.eu>
        [to] => support@karlsbach.eu
        [date] => Thu, 18 Jun 2015 23:05:17 +0300
        [message_id] => <201506182005.t5IK5HoL017863@karlsbach.eu>
        [size] => 4851
        [uid] => 20814
        [msgno] => 20702
        [recent] => 1
        [flagged] => 0
        [answered] => 0
        [deleted] => 0
        [seen] => 0
        [draft] => 0
        [udate] => 1434657920
    )

    [body] => ...
 */
