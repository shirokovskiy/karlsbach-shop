#!/usr/bin/env bash
date
echo "Start Directo import!"
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
KB_DIR="$(pwd)"

STORE_ID=3

# Если число параметров равно 1
if [ $# -eq 1 ]
then
    STORE_ID=$1
    echo "StoreID set to $STORE_ID"
else
    echo "Set Default params"
fi

echo "========================="
echo " Run import from Directo"
echo "========================="
/usr/bin/env php $KB_DIR/shell/directo/import.php $STORE_ID

echo "~~~~~~~~~~~~~~~~~"
echo " Run Reindex All"
echo "~~~~~~~~~~~~~~~~~"
/usr/bin/env php $KB_DIR/shell/indexer.php reindexall
bash $KB_DIR/rmcache.sh
date
