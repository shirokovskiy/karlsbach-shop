#!/usr/bin/env php
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 1/11/15
 * Time         : 5:50 PM
 * Description  : Напоминалка для потенциальных клиентов о забытых корзинах
 */
require_once "../../app/Mage.php";
umask(0);
$app = Mage::app('default');

Mage::setIsDeveloperMode(true);

$storeIds = [1,2,3,4,5];

/* Get customer model, run a query */
$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
/* Let`s check every cart of customer in all stores */
foreach ($collection as $customer) {

    /* Let`s check every Store */
    foreach ($storeIds as $storeId) {
        if (in_array($storeId, [2,3])) {
            $locale = 'en_US';
        } else {
            $locale = 'ru_RU';
        }

        /* Переводчик */
        $translator = Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);

        $app->setCurrentStore($storeId);
        $quote = Mage::getModel('sales/quote')->loadByCustomer($customer);
        $store = $quote->getStore()->load($storeId);
        $quote->setStore($store);

        /**
         * If is there items, send notifications
         */
        if ($quote->getAllItems()) {

            $remindProducts = array();

            /*  */
            foreach ($quote->getAllItems() as $item) {
                #

                $object = $item->getData();
                if (isset($object)) {
                    $out = '';
                    if (is_object($object)) {
                        $classNameOfObj = get_class($object);
                        $out .= $classNameOfObj . "\n";
                        $ms = get_class_methods($object);
                        sort($ms);
                        $out .= print_r($ms, true);
                        $out .= "\n\n";
                    } else {
                        $out .= 'Not object' . "\n";
                        if (is_array($object)) {
                            foreach ($object as $k => $v) {
                                if (is_object($v))
                                    $object[$k] = get_class($v);
                            }
                            $out .= print_r($object, true) . "\n";
                        } else {
                            ob_start();
                            var_dump($object);
                            $vardump_out = ob_get_contents();
                            ob_clean();
                            $out .= $vardump_out . "\n";
                        }
                    }
                    print_r($out);
                    die("\n");
                }

                $productId = $item->getProductId();
                $product = Mage::getModel('catalog/product')->load($productId);

                $remindProducts[$productId]['name'] = $product->getName();
                $remindProducts[$productId]['qty'] = $item->getQty();
                $remindProducts[$productId]['link'] = $product->getProductUrl();
                $remindProducts[$productId]['img'] = $product->getImageUrl();
                $remindProducts[$productId]['article'] = $product->getArticle();
            }

            // Send Email
            try {
                // Transactional Email Template's ID
                if (in_array($storeId, [2,3])) {
                    $templateId = 1;
                } else {
                    $templateId = 3;
                }

                // Set sender information
                $senderName = Mage::getStoreConfig('trans_email/ident_support/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
                $sender = array('name' => $senderName, 'email' => $senderEmail);

                // Set recipient information
                $recipientName = $customer->getData('firstname').' '.$customer->getData('lastname');
                $recipientEmail = $customer->getEmail();

                // Set variables that can be used in email template
                $emailTemplateVariables = array(
                    'customerName' => $recipientName,
                    'customerEmail' => $recipientEmail,
                    'cartProducts' => 'x-x-x-x'
                );

                if (!empty($remindProducts)) {
                    $emailTemplateVariables['cartProducts'] = '<table><tr><th>'.__('Image').'</th><th>'.__('Title').'</th><th width="90">'.__('Article').'</th><th width="60">'.__('Qty').'</th></tr>';
                    foreach ($remindProducts as $pID => $arrPrd) {
                        $emailTemplateVariables['cartProducts'] .= '<tr><td><img src="'.$arrPrd['img'].'" style="width:75px; height: 75px" /></td><td><a href="'.$arrPrd['link'].'">'.$arrPrd['name'].'</a></td><td align="center">'.$arrPrd['article'].'</td><td align="center">'.$arrPrd['qty'].'</td></tr>';
                    }
                    $emailTemplateVariables['cartProducts'] .= '</table>';
                }

                /**
                 * Send Transactional Email
                 *
                 * @var Mage_Core_Model_Email_Template $emailTemplate
                 */
                $emailTemplate = Mage::getModel('core/email_template');

                if ($customer->getId() != 1) {
                    $emailTemplate->addBcc('jaan@karlsbach.eu')
                        ->addBcc('karlsbach@karlsbach.eu')
                        ->addBcc('jimmy.webstudio@gmail.com');
                }

                $emailTemplate->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $emailTemplateVariables, $storeId);

                $translator->setTranslateInline(true);
            } catch (Exception $ex) {
                Mage::log( $ex->getTraceAsString() );
            }
        }
    }

    die("\n");
}

