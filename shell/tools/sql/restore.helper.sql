-- After restore dump locally from production
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE `log_customer`;
TRUNCATE TABLE `log_quote`;
TRUNCATE TABLE `log_summary`;
TRUNCATE TABLE `log_summary_type`;
TRUNCATE TABLE `log_url`;
TRUNCATE TABLE `log_url_info`;
TRUNCATE TABLE `log_visitor`;
TRUNCATE TABLE `log_visitor_info`;
TRUNCATE TABLE `log_visitor_online`;

-- report_viewed_product_index
-- report_compared_product_index
-- report_event
-- catalog_compare_item

UPDATE `core_config_data` SET `value` = 'http://dm.karlsbach.eu/' WHERE `value` LIKE 'http://test.karlsbach.eu/' OR `value` LIKE 'http://shop.karlsbach.eu/' OR `value` LIKE 'https://shop.karlsbach.eu/' OR `value` LIKE 'https://www.karlsbach.eu/' OR `value` LIKE 'http://www.karlsbach.eu/' OR `value` LIKE 'http://karlsbach.eu/' OR `value` LIKE 'https://karlsbach.eu/' LIMIT 2;
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/css/merge_css_files';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/log/active';
UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/head/includes'; -- remove counter scripts and other online services
UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/footer/absolute_footer'; -- remove GTM script
UPDATE `core_cache_option` SET `value` = 0; -- Turn OFF any cache types

SET FOREIGN_KEY_CHECKS = 1;
