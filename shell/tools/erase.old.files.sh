#!/usr/bin/env bash
#
# This script will erase old files

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
DR="$(pwd)"

cd $DR/var/backups
find . -type f -regextype sed -regex ".*/.*_db\.gz" -ctime +14 -exec rm {} \;

cd $DR/var/log
find . -type f -regextype sed -regex ".*/.*\.log" -ctime +14 -exec rm {} \;

cd $DR/var/import/media
find . -type f -regextype sed -regex ".*/.*\.jpg" -mtime +184 -exec rm {} \;
