#!/bin/bash
# Fix it for another project
REMOTE_DBNAME="karlsbach_v2"
REMOTE_DBUSER="karlsbach"
DBNAME="database_development" 					# look at docker-compose.yml for database-name
DBUSER="dbuser"




# Parameters
# 1 - path to dump-file (optional)
# 2 - database name (optional)
date
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RF="$(pwd)"
BACKUPS="$RF/var/backups"

# Если число параметров равно 1 или 2
if [ $# -eq 1 -o $# -eq 2 ]
then
    DBFILE=$1

    if [ $# -eq 2 ]
    then
        DBNAME=$2
    fi
else
    DBFILE=$BACKUPS/$REMOTE_DBNAME.sql
fi

echo "Current folder: $(pwd)"
echo "Dump file: $DBFILE"
echo "Database name: $DBNAME"

if [ ! -f $DBFILE -a -f $DBFILE.bz2 ]
then
    echo "Archive exists > unzip dump"
    cd $BACKUPS
    bunzip2 $DBFILE.bz2
    cd $THIS_DIR
fi

if [ ! -f $DBFILE -a -f $DBFILE.7z ]
then
    echo "Unzip database $FILENAME"
    cd $BACKUPS
    7z x $DBFILE.7z -so > $REMOTE_DBNAME.sql
    cd $THIS_DIR
fi

#echo "Drop database"
#mysql --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf -e "DROP DATABASE $DBNAME;"

#echo "Create database"
#mysql --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf -e "CREATE DATABASE $DBNAME CHARACTER SET utf8 COLLATE utf8_general_ci;"

echo "Fix user access in "$DBFILE
du -sh $DBFILE
sed -i 's/`$REMOTE_DBUSER`@`localhost`/`$DBUSER`@`%`/g' $DBFILE

echo "Restore database from file "$DBFILE" to local MySQL at <"$DBNAME"> database"
pv $DBFILE | mysql --defaults-extra-file=$THIS_DIR"/sql/dockerdb.cnf" -f $DBNAME
pv $THIS_DIR/sql/restore.helper.sql | mysql --defaults-extra-file=$THIS_DIR"/sql/dockerdb.cnf" -f $DBNAME
date
