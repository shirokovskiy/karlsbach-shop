#!/usr/bin/env bash
date
echo "Start getting images!"
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
KB_DIR="$(pwd)"

rsync -uvar --size-only kb:~/www/htroot/media/wysiwyg $KB_DIR/media/

TIME_LIMIT=17
choice=""
echo "Are you sure you want to download catalog category and product images? <y/N>"
read -t $TIME_LIMIT choice

if [ ! -z "$choice" ]
then
    case "$choice" in
      y|Y ) rsync -uvar --exclude=cache --size-only kb:~/www/htroot/media/catalog $KB_DIR/media/;;
      n|N ) echo "Images download skip!";;
      * ) echo "invalid choise :( no RSYNCing";;
    esac
else
    echo "No choice to download images"
fi




bash $KB_DIR/rmcache.sh
date
