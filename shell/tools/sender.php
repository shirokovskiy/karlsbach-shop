<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 18.08.15
 * Time         : 1:25
 * Description  : отправитель HTML-шаблона письма
 * В качестве параметра можно передать имя файла, где будут указаны E-mail-ы получателей построчно.
 *
 * php ./sender.php emails_private.csv >> ./sender-discount30.companies.log &
 *
 * Предварительно слить файл emails_private или emails_company из локальной БД test
 * таблицы `emails_private`, выбрав Email-ы по параметру `type` с нужным значением.
 *
 * Большой файл при необходимости можно обрезать "сверху" командой:
 * sed -i.BAK -n '/vedan@mail.ru/,$ p' emails-big-size.csv
 *
 * Чтобы узнать до какого адреса надо удалять, смотри файл last-email.txt
 * Чтобы увидеть верхние строки большого файла, достаточно выполнить команду:
 * less emails_biglist.csv | head -4
 *
 * P.S. так было раньше!!!! А сейчас...
 *
 * всё работает из БД напрямую.
 * Но всё также учитывается файл last-email.txt
 *
 * Полезные команды:
 * mailq - посмотреть список писем в очереди
 * tail -f /var/log/mail.log
 * sudo ls -lsa /var/spool/mqueue/
 *
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '4096M');

/**
 * Check if process running
 */
$cli = "ps aux | grep -i '".basename(__FILE__)."' | grep -v grep";
exec($cli, $pids);
if (is_array($pids) && !empty($pids)) {
    echo "Process is running already.".PHP_EOL;
    print_r($pids);

    $pids = array_filter($pids, function($value){
        return !preg_match("/\.log/i", $value);
    });

    if (count($pids) > 1) {
        echo "Filtered processes:".PHP_EOL;
        print_r($pids);
        echo "Stop execution current.".PHP_EOL;
        die();
    }
}

$thisDir = __DIR__;

$file_name = 'last-email.txt';
$isCustomList = false;

// В командной строке указать нужный файл с адресами
if (isset($argv[1]) && !empty($argv[1])) {
    $file_name_my_list = (string)$argv[1];

    $isCustomList = true;
}

include "cls.mail.php";
$cMail = new clsMail('Sandbox-'.date('YmdHi'));
$mailSender = 'support@karlsbach.eu';
$mailSenderName = 'Karlsbach LLC';
$sendFrom[] = $mailSender;
$sendFrom[] = $mailSenderName;

$msgSubject = 'NEW • Wooden Decor and Furniture';
$msgInText = file_get_contents(__DIR__."/message.txt");
$msgInHtml = file_get_contents(__DIR__."/message.html");

$last_email = null;

if (file_exists(__DIR__ . DIRECTORY_SEPARATOR .$file_name)) {
    $last_email = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .$file_name);
}

$user = 'root';
$pass = '1234qaz';
$sql = "SELECT `email` FROM `emails_private` WHERE `type` != 'ignored' ".(!empty($last_email)?"AND `email`>'".trim($last_email)."'":'')." ORDER BY `email` LIMIT 3000";

if ($isCustomList) {
    $sql = "SELECT 'jimmy.webstudio@gmail.com' as `email` UNION ALL SELECT 'dshirokovskiy@gmail.com' as `email` UNION ALL SELECT 'shirokovskij@yahoo.com' as `email` UNION ALL SELECT 'jimmya@yandex.ru' as `email` UNION ALL SELECT 'd.shirokovskiy@aupontrouge.ru' as `email`";
}

try {
    $dbh = new PDO('mysql:host=localhost;dbname=test', $user, $pass);
    $Z = 0;
    foreach($dbh->query($sql) as $row) {
        $sendTo = trim($row['email']);
        if ($cMail->validate_mail($sendTo)) {
            $msgInHtml = str_replace('#EMAIL#', $sendTo, $msgInHtml);

            $cMail->new_mail( $sendFrom, $sendTo, $msgSubject, $msgInText, $msgInHtml );

            if ( $cMail->send() !== true ) {
                echo 'Not sent to '.$sendTo, "\n";
            } else {
                echo '#'.$Z.' Sent to '.$sendTo, "\n";
                if (!$isCustomList) {
                    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . $file_name, $row['email']);
                    //usleep(5);
                }
            }
        } else {
            echo "Плохой:".$sendTo,"\n";
        }
        $Z++;
    }
    $dbh = null;
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

echo "Finished at ". date(DATE_RFC2822), PHP_EOL;
