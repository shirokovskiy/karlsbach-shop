#!/usr/bin/env bash
date
echo "Start getting dump!"
DBNAME=karlsbach_v2
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RF="$(pwd)"
FILENAME=$DBNAME.sql.7z
BACKUPS=$RF"/var/backups"

echo "Download database dump"
rsync -Lpgo kb:~/www/v2/var/backups/$FILENAME $BACKUPS/

if [ -f $BACKUPS/$FILENAME ]
then
    rm -f $BACKUPS/$DBNAME.sql
    echo "Unzip database"
    cd $BACKUPS
    7z x $FILENAME -so > $DBNAME.sql
else
    echo "No file "$FILENAME
fi
