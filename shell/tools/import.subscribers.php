#!/usr/bin/env php
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 20.03.15
 * Time         : 23:01
 * Description  : Import list of subscribers
 *
 * php ./import.subscribers.php [filename.csv]
 *
 * Файл subscribers.csv необходимо предварительно подготовить при помощи Libre Office Calc например.
 * Экспортируем таблицу адресов из MySQL в CSV (1 столбец)
 * Открываем файл csv на редактирование и добавляем в начало строку
 * "Электронная почта","Статус","ID магазина"
 *
 * В поле "Статус" ставим цифру "Подписаны" = 1 (см. if-ы в коде)
 * В поле "ID магазина" - ставим цифру из раздела Управление магазинами для конкретного View (это и есть StoreID).
 *
 * Используя короткие клавиши Office Calc:
 * ткнуть мышкой в первое значимое поле, которое хотим дублировать,
 * далее Ctrl + Shift + End, не отпуская Shift стрелочками выделяем только нужный столбец,
 * нажимаем Ctrl + D (дублирование записей вниз).
 * Формат смотри ниже.
 */
$store_id = 1;
$csv_filepath = "subscribers.csv";
$csv_delimiter = ',';
$csv_enclosure = '"';
$magento_path = dirname(dirname(__DIR__));

if (isset($argv[1]) && !empty($argv[1])) {
    $csv_filepath = (string)$argv[1]; // имя файла в текущей директории
}

require "{$magento_path}/app/Mage.php";
$global_store_id = $store_id;
Mage::app()->setCurrentStore($store_id);
echo "\n";
$fp = fopen(__DIR__.DS.$csv_filepath, "r");

if (!$fp) die("{$csv_filepath} not found\n");
$count = 0;
$validatorEmail = new Zend_Validate_EmailAddress();

/**
 * CSV Format: такой же как если експортировать подписчиков из CMS
 */

while (($row = fgetcsv($fp, 0, $csv_delimiter, $csv_enclosure)) !== false){
    $col = 0;
    $email = trim($row[$col]);  // Email
    $status = trim($row[++$col]); // "Подписаны" == 1 (см.обработку статусов ниже)
    $store_id = trim($row[++$col]); // STORE_ID = 1 or 2 or .... "Зоомагазин" / "Russian Store View" (брать из раздела Система - Управление магазинами - Название представления магазина)

    if ($validatorEmail->isValid($email)) {
        // email appears to be valid
    } else {
        echo sprintf("%7d",$count), sprintf("%256s" ,$email), ' bad', "\n";
        continue;
    }

    $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
    if ($subscriber->getId()){
        echo sprintf("%7d",$count), sprintf("%256s" ,$email) . " already subscribed\n";
        continue;
    }

    if ( intval($store_id) > 0 && $global_store_id != $store_id ) {
        echo "Set new store $store_id\n";
        $global_store_id = $store_id;
        Mage::app()->setCurrentStore($store_id);
    }

    Mage::getModel('newsletter/subscriber')->setImportMode(true)->subscribe($email);
    $subscriber_status = Mage::getModel('newsletter/subscriber')->loadByEmail($email);

    if ($status == 1){
        $subscriber_status->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
        $subscriber_status->save();
    }else if($status == 2){
        $subscriber_status->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE);
        $subscriber_status->save();
    }else if($status == 3){
        $subscriber_status->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED);
        $subscriber_status->save();
    }else if($status == 4){
        $subscriber_status->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNCONFIRMED);
        $subscriber_status->save();
    }
    echo sprintf("%7d",$count), sprintf("%256s" ,$email)," ok\n";

    $count++;
}
fclose($fp);
rename($csv_filepath, 'z_imported_'.$csv_filepath);
echo "Import finished\n";
