#!/usr/bin/env bash
date
echo "Start send newsletter!"
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RD="$(pwd)"
cd $THIS_DIR

# Detect ROOT user
AS_ROOT=0
if [[ $EUID -eq 0 ]]; then
   echo "This script has been run under root user "$EUID
   AS_ROOT=1
fi

if [[ $AS_ROOT -eq 1 ]];
then
    echo "Remove old mail queues"
    # -mtime параметр дней
    # -mmin  параметр минут
    service sendmail stop
    find /var/spool/mqueue/ -type f -mmin +90 -exec rm {} \;
    service sendmail start
fi

# Если число параметров равно 1 или 2
if [ $# -eq 1 -o $# -eq 2 ]
then
    php $THIS_DIR/sender.php $1
else
    php $THIS_DIR/sender.php
fi



