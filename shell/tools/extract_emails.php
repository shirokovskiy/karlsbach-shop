#!/usr/bin/env php
<?php
/**
 * Скрипт вытягивает Email адреса из текстовых файлов (файлы должны лежать тут же)
 * Кладёт в БД test (рассчитан на запуск на localhost)
 * В строке вставки в таблицу адресов можно поправить название целевой таблицы (строка ~68)
 * Скрипт запускать в CLI:
 * ./extract_emails.php [file-name.txt]
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

$file_name = 'some.sql';
$kBytes = 6;

if (isset($argv[1]) && !empty($argv[1])) {
    $file_name = (string)$argv[1];
}
if (isset($argv[2]) && !empty($argv[2])) {
    $kBytesTmp = (int)$argv[2];
    if ($kBytesTmp >0 || $kBytesTmp <= 20) $kBytes = $kBytesTmp;
    unset($kBytesTmp);
}

if (!file_exists($file_name)) {
    die ("File doesn't exist: ".$file_name."\n");
}

$gc = 0;

try {
    $dbh = new PDO('mysql:host=localhost;dbname=test', 'root', '1234qaz');
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    print "PDO Error: " . $e->getMessage() . "\n";
    die("\n");
}

$DR = dirname(__FILE__);
$filenameLog = $DR.'/extract_emails.log';
$filenameDup = $DR.'/duplicate.emails.txt';

file_put_contents($filenameLog, "Start scenario".date('j F H:i:s')."\n", FILE_APPEND);
$time_start = microtime(true);
$previous_line_part = '';

$source_file = fopen( $file_name, "r" ) or die("Couldn't open $file_name");
if ($source_file)
while (!feof($source_file)) {
    $line = fread($source_file, (1024 * $kBytes));
    if ($line === false) {
        throw new Exception("File read error");
    }
    if (!empty($previous_line_part))
        $line = $previous_line_part.$line;
    $previous_line_part = substr($line,-24);

    ++$gc;
    $matches = array();

    if (false === preg_match_all('/[[:alnum:]][\w\.-_]{0,100}@[\w\.-]+\.[[:alpha:]]{2,6}/i', $line, $matches)) {
        file_put_contents($filenameLog, "$gc - Line export failed\n", FILE_APPEND);
        continue;
    }
    if (!isset($matches[0][0])) {
        // echo "No data in $gc line.\n";
        continue;
    } else $count_emails = count($matches[0]);

    foreach($matches[0] as $email) {
        try {
            $handle = $dbh->prepare("INSERT INTO emails_private (`email`,`type`) values (:email,'private')");
//        $handle->bindValue(1, 17, PDO::PARAM_INT);
            $handle->bindValue(':email', $email);
            $handle->execute();
        } catch (PDOException $e) {
            $errorInfo = $handle->errorInfo();

            if ($errorInfo[1] == 1062) {
                // inform user, throw a different exception, etc
                file_put_contents($filenameDup, $email."\n", FILE_APPEND);
            } else {
//                throw $e; // a different error, let this exception carry on

                print "PDO Error: " . $e->getMessage() . "\n";
                die("\n");
            }
        }
    }
    $time_current = microtime(true);
    file_put_contents($filenameLog, 'Iteration #'.sprintf("%7d", $gc).' '. sprintf("%6.2f", ($time_current - $time_start)) . " sec. & found ".sprintf("%4d", $count_emails)." emails with previous line: $previous_line_part\n", FILE_APPEND);
}
fclose($source_file);
rename($file_name, 'z_done_'.$file_name);
file_put_contents($filenameLog, 'The End of '.$file_name.' at '.date('H:i:s')."!\n", FILE_APPEND);
