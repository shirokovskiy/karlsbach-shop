#!/usr/bin/env bash
date
DBUSER=karlsbach
DBNAME=karlsbach_shop
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
KB_DIR="$(pwd)"
cd $THIS_DIR
BKP_DIR=$KB_DIR/var/backups
BKP_FILE=$DBNAME.local-dump.$CUR_DATE.sql
DBFILE=$DBNAME.sql
FILENAME=$DBFILE.7z
DESTINATION=$BKP_DIR/$DBFILE
DESTINATION_ZIP=$BKP_DIR/$FILENAME
DESTINATION_BKP=$BKP_DIR/$BKP_FILE

if [ -f $DESTINATION_ZIP ]
then
    cd $BKP_DIR
    echo "Start unzip: $DESTINATION_ZIP"
    7z e $DESTINATION_ZIP
else
    echo "There is no downloaded file: $DESTINATION_ZIP"
fi

echo "Make local database backup"
mysqldump --defaults-extra-file=$THIS_DIR/custom.cnf --opt $DBNAME > $DESTINATION_BKP

if [ -f $DESTINATION ]
then
    echo "Run DB restore process"
    mysql --defaults-extra-file=$THIS_DIR/custom.cnf $DBNAME < $DESTINATION
else
    echo "There is NO remote dump file: $DESTINATION"
    exit 1
fi

echo "Run DB updates"
mysql --defaults-extra-file=$THIS_DIR/custom.cnf $DBNAME < $BKP_DIR/remote.dump.helper.sql
mv -f $DESTINATION $DESTINATION.prod.$CUR_DATE.sql

bash $KB_DIR/rmcache.sh
date
