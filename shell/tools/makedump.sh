#!/bin/bash
date
echo "Start making dump!"
DBNAME=karlsbach_shop
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RD="$(pwd)"
cd $THIS_DIR
FILENAME=$DBNAME.$CUR_DATE.sql
BACKUPS=$RD/var/backups
LOCAL_DUMP=$DBNAME.sql.7z
find $BACKUPS -name '*.sql.7z' -type f -mtime +7 -exec rm {} \;

if [ -f $BACKUPS/$FILENAME ]
then
    echo "Remove previous dump file "$FILENAME
    rm -f $BACKUPS/$FILENAME
fi

echo "Make dump of "$DBNAME" into "$FILENAME
mysqldump --defaults-extra-file=$RD/shell/tools/custom.cnf --opt $DBNAME > $BACKUPS/$FILENAME

if [ -f $BACKUPS/$FILENAME ]
then
    echo "Archive dump"
    7z a -mx=9 $BACKUPS/$FILENAME.7z $BACKUPS/$FILENAME
    rm -rf $BACKUPS/$FILENAME
fi


if [[ -h $BACKUPS/$LOCAL_DUMP ]] || [[ -f $BACKUPS/$LOCAL_DUMP ]]
then
    echo "Remove to repair symbolic link"
    rm -rf $BACKUPS/$LOCAL_DUMP
else
	echo "No such file "$BACKUPS/$LOCAL_DUMP
fi

cd $BACKUPS
if [ -f $FILENAME.7z ]
then
    echo "Make new link $LOCAL_DUMP -> $FILENAME.7z"
    ln -s $FILENAME.7z $LOCAL_DUMP
fi

echo "The End of making dump!"
date
