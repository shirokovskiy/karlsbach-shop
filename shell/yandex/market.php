<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 01.04.17
 * Time         : 14:20
 * Description  : Prepare YML-file for Yandex.Market bot
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL &~ E_STRICT &~ E_NOTICE &~ E_DEPRECATED);
umask(0);
ini_set('memory_limit', '4096M');
//opcache_invalidate(__FILE__, true);
chdir(dirname(dirname(dirname(__FILE__))));
proc_nice(20);

require 'app/Mage.php';

if (!Mage::isInstalled()) {
    echo "Application is not installed yet, please complete install wizard first.";
    exit;
}

$storeId = 1; // Russian Store
$locale = "ru_RU";

Mage::app('admin')->setUseSessionInUrl(false);
#Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);
Mage::app()->setCurrentStore(Mage::app()->getStore($storeId));

try {
    $products = Mage::getResourceModel('catalog/product_collection');
    $products
        ->addAttributeToSelect('*')
        ->joinField('qty',
            'cataloginventory/stock_item',
            'qty',
            'product_id=entity_id',
            '{{table}}.stock_id=1 AND qty > 0',
            'inner')
//        ->addAttributeToFilter('qty', array("gt" => 0))
        ->addAttributeToFilter('type_id', 'simple')
        ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
    ;
    $products->getSelect()->order("e.entity_id desc");
} catch (Exception $ex) {
    echo "Something goes wrong. See exception.log". PHP_EOL;
    Mage::logException($ex);
}

$_dirSource = BP . DS . 'var' . DS . 'import';
$_fileSource = 'tmp_products_exported.xml';
$fileSource = 'products_exported.xml';

echo "Start at ", date("d.m.Y H:i:s"), PHP_EOL;

if ($products->count()) {
    echo "Total products = ", $products->count() . PHP_EOL;

    // Start create file
    $string = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>".PHP_EOL;
    file_put_contents($_dirSource.DS.$_fileSource, $string );

    $string = "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">".PHP_EOL;
    $string .= "<yml_catalog date=\"".date("Y-m-d H:i")."\">".PHP_EOL;
    $string .= "<shop>".PHP_EOL;
    $string .= "<name>Karlsbach</name>".PHP_EOL; // Name of shop
    $string .= "<company>".mb_convert_encoding("Karlsbach – это широкий ассортимент изделий для интерьера: искусственные цветы, деревья, металлические вазы, аксессуары для дома, новогодние игрушки, ели - ёлки - ёлочки, светодиодные гирлянды, прожекторы и проекционное оборудование, которые станут стильным украшением каждого дома. Мы поможем украсить дом новогодним декором. Также есть свадебный декор. Наши дизайнеры помогут с оформлением. Мы оказываем помощь архитектурным и дизайнерским студиям.", "windows-1251", "utf-8")."</company>".PHP_EOL;
    $string .= "<url>http://shop.karlsbach.eu</url>".PHP_EOL;
    $string .= "<currencies><currency id='RUR' rate='1' plus='0'/></currencies>".PHP_EOL;
    file_put_contents($_dirSource.DS.$_fileSource, $string, FILE_APPEND);
    ////////////////////////////////


    // Categories
    $string = "<categories>".PHP_EOL;

    // todo: fetch 1-level categories programmaticaly
    $mainCats = [3,5,7,120,170,215]; // Main Shop Categories

    $knownCats = [];

    foreach ($mainCats as $catId) {
        $_main_category = Mage::getModel('catalog/category')->load($catId);

        $string .= categoriesNodes($_main_category);
    }
    $string .= "</categories>".PHP_EOL;
    $string .= "<local_delivery_cost>2250</local_delivery_cost>".PHP_EOL;
    file_put_contents($_dirSource.DS.$_fileSource, $string, FILE_APPEND);
    ////////////////////////////////


    // Products
    $string = "<offers>".PHP_EOL;
    file_put_contents($_dirSource.DS.$_fileSource, $string, FILE_APPEND);

    foreach($products as $product){
        $product->load();
        if ($product->getIsSalable()!=1 || $product->getIsInStock()!=1) {
            echo "Product is not salable! Product ID = ". $product->getId(). PHP_EOL;
            continue;
        }

        // Find out which Category
        $arrPrdCats = $product->getCategoryIds();
        $prdCatId = 0;
        if (is_array($arrPrdCats) && !empty($arrPrdCats)) {
            $arrPrdCats = array_reverse($arrPrdCats);
            foreach ($arrPrdCats as $index => $prdCat) {
                if (in_array($prdCat, $knownCats)) {
                    $prdCatId = (int) $prdCat;
                    break;
                }
            }
        }
        if (!$prdCatId) {
            echo "Product not in any Category! Product ID = ". $product->getId(). PHP_EOL;
            continue;
        } // if Category unknown, skip this Product =(

        // The beginning product publication
        $string = "<offer id='{$product->getId()}' type='vendor.model' available='".($product->getStatus() != "1"?"false":"true")."'>".PHP_EOL;
        $string .= "<url>{$product->getProductUrl()}</url>".PHP_EOL;
        $string .= "<price>{$product->getFinalPrice()}</price>".PHP_EOL;
        $string .= "<currencyId>RUR</currencyId>".PHP_EOL;
        $string .= "<categoryId>$prdCatId</categoryId>".PHP_EOL;

        // Images of Product
        $allowedPictures = 5; $currentProductPictures = 0;
        foreach ($product->getMediaGalleryImages() as $image) {
            $currentProductPictures++;
            $string .= "<picture>".Mage::helper("catalog/image")->init( $product, "thumbnail", $image->getFile() )->resize(600, 800)."</picture>".PHP_EOL;
            if ($currentProductPictures>=$allowedPictures) break;
        }

        $string .= "<delivery>true</delivery>".PHP_EOL;
        $string .= "<vendor>Karlsbach</vendor>".PHP_EOL;
        $string .= "<vendorCode>{$product->getSku()}</vendorCode>".PHP_EOL;
        $string .= "<model>".xmlEscape($product->getName())."</model>".PHP_EOL;

        if (strlen($product->getDescription()) >= 10 && $product->getDescription() != $product->getName()) {
            $string .= "<description>".xmlEscape($product->getDescription())."</description>".PHP_EOL;
        }

        $string .= "<sales_notes>Скидки при крупных оптовых заказах.</sales_notes>".PHP_EOL;

        if ($product->getColor()) {
            $string .= "<param name='Цвет'>".xmlEscape($product->getResource()->getAttribute('color')->getFrontend()->getValue($product))."</param>".PHP_EOL;
        }

//        $string .= "<param name='Сезон'>".$product->getResource()->getAttribute('season')->getFrontend()->getValue($product)."</param>".PHP_EOL;
//        $string .= "<param name='Пол'>".__($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product))."</param>".PHP_EOL;
        $string .= "</offer>".PHP_EOL;

        file_put_contents($_dirSource.DS.$_fileSource, mb_convert_encoding($string, "windows-1251", "utf-8"), FILE_APPEND);

        if ($iii++ > 1000) break;
    }

    $string = "</offers>".PHP_EOL;
    $string .= "</shop>".PHP_EOL;
    $string .= "</yml_catalog>".PHP_EOL;
    file_put_contents($_dirSource.DS.$_fileSource, $string, FILE_APPEND);
    ////////////////////////////////

    if (file_exists(BP . DS . $fileSource)) unlink( BP . DS . $fileSource );
    if (!copy($_dirSource . DS . $_fileSource, BP . DS . $fileSource)) {
        echo "Copy Error!".PHP_EOL;
    }
}

echo "The End at ", date("d.m.Y H:i:s"), PHP_EOL;


function categoriesNodes($parentCategory)
{
    global $knownCats;

    if ($parentCategory->getId() && $parentCategory->getIsActive()==1)
    {
        /**
         * Считаем все товары категории и её подкатегорий
         */
        if ($parentCategory->hasChildren()) {
            $categoryLimit = explode(',',$parentCategory->getAllChildren());
        } else {
            $categoryLimit = $parentCategory->getId();
        }
        //
        $productCollection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(0)
            ->addAttributeToSelect('*')
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'inner')
            ->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1 AND qty > 0', 'inner')
//            ->addAttributeToFilter('qty', array("gt" => 0))
            ->addAttributeToFilter('category_id', array('in' => $categoryLimit))
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('type_id', 'simple');
        $productCollection->getSelect()->group('entity_id')->distinct(true);
        $productCollection->load();

        $intCountCatProducts = $productCollection->count();
        if ($intCountCatProducts <= 0) return false;

        $knownCats[] = $parentCategory->getId();

        $string = "<category id='".$parentCategory->getId()."'".($parentCategory->getLevel() > 2?" parentId='".$parentCategory->getParentId()."'":"").">".mb_convert_encoding($parentCategory->getName(), "windows-1251", "utf-8")."</category>".PHP_EOL;

        foreach ($parentCategory->getChildrenCategories() as $subCategory) { // только дочерние (не внучатые)
            $string .= categoriesNodes($subCategory);
        }

        return $string;
    }

    return false;
}

function mb_ucfirst($string)
{
    return mb_strtoupper(mb_substr($string, 0, 1)).mb_strtolower(mb_substr($string, 1));
}

function xmlEscape($string) {
    return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
}
