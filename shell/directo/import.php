#!/usr/bin/env php
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 12/31/13
 * Time         : 11:36 AM
 * Description  : Import products from remote Directo ERP
 * Every time you run this script it requests data from remote server
 * Run this script in CLI (crontab)
 * Example: php -f import.php [store_id] [count_products] [reset_exist_products]
 */

require_once dirname(__FILE__)."/../../app/Mage.php";
umask(0);

/**
 * Основная настройка окружения
 */
$store_id = 3; // 3 = preorder, 2 = order (stock), 6 = spring preorder

if (isset($argv[1]) && in_array($argv[1], array(2,3,6))) {
    $store_id = (int)$argv[1];
}

$import_log_filename = 'import.from.directo.store.'.$store_id.'.'.date('Y-m-d_H:i').'.log';
Mage::log( 'Import products for Store ID: '.$store_id.' ('.date('H:i:s').")", null, $import_log_filename);

$website_ids = array($store_id);
$store_cat_deps = array(2=>20, 3=>21, 6=>149); // dependencies of store to the root category IDs
$store_status_deps = array(2=>0, 3=>1, 6=>1); // dependencies of store to the product status [order/preorder/spring]
$store_default_category_id = $store_cat_deps[$store_id]; // 21 = preorder root category, 20 = order
$app = Mage::app();
$app->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

/**
 * Включаем возможность логировать
 */
ini_set('display_errors', 1);
define('MY_DEBUG_MODE', false);
Mage::setIsDeveloperMode(true); // выключить на продакшн

Mage::log( 'App prepared! ('.date('H:i:s').")", null, $import_log_filename);

/**
 * Create variables
 */
$arrExistCategories = array();
$xmlstr = '';

/**
 * Для отладки (ограничения) по нескольким продуктам только
 * @example ./shell/directo/import.php 2 1
 */
$debug_iter_num = 0;
if (isset($argv[2]) && intval($argv[2]) > 0) {
    $debug_iter_num = (int)$argv[2];
    Mage::log( '$debug_iter_num: '.$debug_iter_num, null, $import_log_filename);
}

$reset_previous = true;
if (isset($argv[3]) && intval($argv[3]) > 0) {
    $reset_previous = false;
    Mage::log( '$reset_previous: false', null, $import_log_filename);
}

/**
 * Main Stream
 * https://directo.gate.ee/xmlcore/bottom_line_technologies/xmlcore.asp
 *
 * Special Key (appkey)
 * B2F846D43830F4509BE27534C71C169C
 *
 * Minimum parameters
 * get=1
 * what=datatype  (item|customer)
 * key=appkey
 */
// Counters
$i /*inserted*/ = $u /*updated*/= $g /*global counter*/= 0;
$xmlstr = '';
$uri = 'https://directo.gate.ee/xmlcore/bottom_line_technologies/xmlcore.asp?get=1&what=item&key=B2F846D43830F4509BE27534C71C169C&timestamp='.time();
//$uri = ''; // Set this variable, and copy XML 'xmlcore_htm.xml' locally into root folder of project
try {
    if (!empty($uri)) {
        if (MY_DEBUG_MODE) Mage::log( 'Get XML by URL '.$uri, null, $import_log_filename);
        $xmlstr = get_url_contents($uri);
    }

    if (MY_DEBUG_MODE) Mage::log( 'Got XML', null, $import_log_filename);
    libxml_use_internal_errors(true);

    if (empty($xmlstr)) {
        if (MY_DEBUG_MODE) Mage::log( 'remote XML Data is empty. Read from LOCAL file.', null, $import_log_filename);
        /*if ('vmint' == php_uname('n') || 'SVE1512Y1RB' == php_uname('n')) {*/
        $xmlstr = file_get_contents('xmlcore_htm.xml'); // DEBUG MODE
        /*}*/
    } else {
        file_put_contents(Mage::getBaseDir('var').DS.'import'.DS.'xmlcore_htm.'.date('Ymd.His').'.xml', $xmlstr);
    }

    $xml = simplexml_load_string($xmlstr);
    unset($xmlstr);

    if (MY_DEBUG_MODE) Mage::log( 'Load XML', null, $import_log_filename);
    if (!$xml) {
        Mage::log('Error loading XML from '.$uri, null, $import_log_filename);
        foreach(libxml_get_errors() as $error) {
            Mage::log( $error->message , null, $import_log_filename);
        }
    } else {
        /**
         * Parse XML into Products info
         *
         * $xml is object
         */
        if (isset($xml->items) && is_object($xml->items)) {
            if ($reset_previous && $debug_iter_num <= 0) {
                /**
                 * Сначала сбросим все существующие товары
                 * Возможно какие-то товары уже не актуальны
                 */
                Mage::log('Prepare all Store products for clearing!', null, $import_log_filename);

                $_ProductCollection = Mage::getModel('catalog/product')
                    ->setStoreId($store_id)
                    ->getResourceCollection()
                    ->addAttributeToSelect('id')
                    ->addAttributeToFilter(
                        'status',
                        array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                    )
                    ->addStoreFilter($store_id);

                Mage::log('Founded products: '.$_ProductCollection->getSize(), null, $import_log_filename);

                if ($_ProductCollection->getSize() > 0) {
                    foreach($_ProductCollection as $_product)
                    {
                        $entity_id = $_product->getId();
                        $product = Mage::getModel('catalog/product')->load($entity_id);

//                        usleep(500000);

                        if ($product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                            Mage::log("Skip product ".$entity_id." because Disabled", null, $import_log_filename);
                            continue;
                        } else {
                            if ($product->getData('is_in_stock') == 0) {
                                Mage::log("Skip product ".$entity_id." because is_in_stock = 0", null, $import_log_filename);
                                continue;
                            } else {
                                $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
//                            $product->addAttributeUpdate('status', Mage_Catalog_Model_Product_Status::STATUS_DISABLED, $store_id);
                                $product->setStockData(array(
                                    'is_in_stock' => 0,
                                    'qty' => 0
                                ))->save();
                                Mage::getModel('catalog/product_status')->updateProductStatus($product->getId(), $store_id, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
                                Mage::log("Disable product ".$entity_id, null, $import_log_filename);
                            }
                        }
                    }
                    Mage::log('Products is cleared!', null, $import_log_filename);
                }
                unset($_ProductCollection);
            } else {
                Mage::log('Products is not cleared!', null, $import_log_filename);
            }

            $t = 1;
            $dirMageMediaGalleryPath = Mage::getBaseDir('media').DS.'catalog'.DS.'product'.DS.'d'.DS.'i';
            $dirLocalPath = Mage::getBaseDir('media').DS.'import';

            /**
             * Цикл по импортируемым товарам
             */
            foreach ($xml->xpath('//item') as $item) { // for help: http://chris.photobooks.com/xml/default.htm

                if ($debug_iter_num> 0 && $g >= $debug_iter_num) {
                    Mage::log('STOP on '.$g.' product!', null, $import_log_filename);
                    break;
                } // DEBUG MODE

                /**
                 * Create new product
                 */
                $Product = Mage::getModel('catalog/product');
                $t++;
                $id = $article = $sku = null;

                if ($store_id == 3) {
                    /**
                     * Проверить, преордер или нет?
                     */
                    $find = ".//data[@code='PRE_ORDER'][@content='".$store_status_deps[$store_id]."']";
                    $preorder_status = $item->xpath($find);
                    Mage::log("Find xpath: (step $t) ".$find, null, $import_log_filename);

                    if (empty($preorder_status)) {
                        Mage::log('Product '.strval($item->attributes()->code).' is not for StoreID: '.$store_id, null, $import_log_filename);
                        continue;
                    }
                } elseif ($store_id == 6) {
                    /**
                     * Проверить, преордер или нет?
                     */
                    $find = ".//data[@code='PRE_ORDER_SPRING'][@content='".$store_status_deps[$store_id]."']";
                    $preorder_status = $item->xpath($find);
                    Mage::log("Find xpath: (step $t) ".$find, null, $import_log_filename);

                    if (empty($preorder_status)) {
                        Mage::log('Product '.strval($item->attributes()->code).' is not for StoreID: '.$store_id, null, $import_log_filename);
                        continue;
                    }
                }


                /**
                 * Set specific attributes
                 */
                if (isset($item->attributes()->code) && !empty($item->attributes()->code)) {
                    $article = strval($item->attributes()->code); // наименование
                    $sku = 'directo_s'.$store_id.'_'.$article;
                    $id = (int)$Product->getIdBySku($sku);

                    // проверить, нет ли такого продукта уже?
                    if ($id > 0) {
                        $Product = $Product->load($id);

                        Mage::log('Product exist with ID: '.$id." (let`s update)", null, $import_log_filename);

                        if (!$Product->getId()) {
                            Mage::log('Cant load product by ID: '.$id, null, $import_log_filename);
                            continue;
                        }
                    } else {
                        /**
                         * Set default product info
                         */
                        $Product->setTypeId('simple');
                        $Product->setWeight(1);
                        $Product->setAttributeSetId(4); // "Default" in Magento
                        $Product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
                        $Product->setTaxClassId(0);
                        $Product->setCreatedAt(strtotime('now'));
                        $Product->setSku($sku);
                        $Product->setArticle($article);

                        Mage::log('Set New Product', null, $import_log_filename);
                    }

                    $Product->setWebsiteIDs($website_ids);
                    $Product->setStoreId($store_id);
                    $Product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
                    $Product->setData('is_salable', '1');
                } else {
                    Mage::log('No CODE (Article) for the record '.$sku.': '.$item->saveXML(), null, $import_log_filename);
                    continue;
                }

//                if (MY_DEBUG_MODE) Mage::log( 'Line: '.__LINE__, null, $import_log_filename);

                /**
                 * Проверить количество на складе
                 */
                if ($store_id == 2 || $store_id == 6) {
                    $Product->setStockData(array(
                        'is_in_stock' => 1,
                        'qty' => 777
                    ));
                } elseif ($store_id == 3) {
                    $Product->setStockData(array(
                        'is_in_stock' => 1,
                        'qty' => 99999999
                    ));
                } else {
                    $product_qty = $item->xpath(".//stocklevel[@stock='WC'][@level>0]");

                    if (empty($product_qty)) {
                        /**
                         * Возможно есть ещё данные по складу в СПб
                         */
                        $product_qty = $item->xpath(".//data[@code='IN_STP'][@content>0]");

                        if (empty($product_qty)) {
                            $strLog = 'SKIP: Bad product qty for SKU as for Stock as for STP: '.$sku;
                            Mage::log($strLog, null, $import_log_filename);
                            continue;
                        }
                    }

                    if (isset($product_qty[0]) && is_array($product_qty) && !empty($product_qty[0])) {
                        $product_qty = $product_qty[0];
                    }
                    if (isset($product_qty->attributes()->level) && !empty($product_qty->attributes()->level)) {
                        $Product->setStockData(array(
                            'is_in_stock' => 1,
                            'qty' => intval($product_qty->attributes()->level)
                        ));
                        Mage::log('Set qty from Stock', null, $import_log_filename);
                    } elseif (isset($product_qty->attributes()->content) && !empty($product_qty->attributes()->content)) {
                        $Product->setStockData(array(
                            'is_in_stock' => 1,
                            'qty' => intval($product_qty->attributes()->content)
                        ));
                        Mage::log('Set qty from STP', null, $import_log_filename);
                    } else {
                        $strLog = 'SKIP: Absolutely wrong product qty for SKU '.$sku;
                        Mage::log($strLog, null, $import_log_filename);
                        continue;
                    }
                }

                if (isset($item->attributes()->package1) && !empty($item->attributes()->package1)) {
                    $Product->setData('pack_qty', intval($item->attributes()->package1));
                } else {
                    $Product->setData('pack_qty', 1);
                }

                /**
                 * Наименование товара
                 */
                if (isset($item->attributes()->name) && !empty($item->attributes()->name)) {
                    $Product->setName(strval($item->attributes()->name));
                    $Product->setDescription(strval($item->attributes()->name));
                    $Product->setShortDescription(strval($item->attributes()->name));
                } else {
                    Mage::log('No NAME for the record '.$sku.': '.$item->saveXML(), null, $import_log_filename);
                    continue;
                }

//                if (MY_DEBUG_MODE) Mage::log( 'Line: '.__LINE__, null, $import_log_filename);

                /**
                 * Получаем штрихкод
                 */
                if (isset($item->attributes()->barcode) && !empty($item->attributes()->barcode)) {
                    $Product->setData('barcode', strval($item->attributes()->barcode));
                } else {
                    Mage::log('No BARCODE for the record '.$sku.': '.$item->saveXML(), null, $import_log_filename);
                    continue;
                }

//                if (MY_DEBUG_MODE) Mage::log( 'Line: '.__LINE__, null, $import_log_filename);

                /**
                 * Получаем цену
                 */
                if (isset($item->attributes()->price) && !empty($item->attributes()->price)) {
                    $Product->setPrice(floatval($item->attributes()->price));
                } else {
                    Mage::log('No PRICE for the record '.$sku.': '.$item->saveXML(), null, $import_log_filename);
                    continue;
                }

                /**
                 * Перекачиваем картинки
                 */
                if (isset($item->attributes()->url) && !empty($item->attributes()->url)) {
                    /**
                     * To download image for product
                     */
                    $imgSize = $imgLocalPath = null;

                    Mage::log('Get remote image from: '.$item->attributes()->url, null, $import_log_filename);

                    if (!empty($article)) {
                        $imgUrl = strval($item->attributes()->url);
                        try {
                            $imgSize = getimagesize($imgUrl);
                        } catch (Exception $ex) {
                            Mage::log($ex->getTraceAsString(), null, $import_log_filename);
                        }

                        if (is_array($imgSize) && !empty($imgSize)) {
                            // удалённая картинка есть (в Директо)
                            try {
                                if (!is_dir($dirLocalPath)) {
                                    $file = new Varien_Io_File();
                                    $file->mkdir($dirLocalPath);
                                    unset($file);
                                }
                                $fileName = 'directo.'.$store_id.'.'.$article.'.jpg';
                                $imgLocalPath = $dirLocalPath.DS.$fileName;
                                Mage::log('Copy remote image', null, $import_log_filename);

                                //$imgUrl = $dirLocalPath.DS.'dima.test.jpg';

                                if (file_exists($imgLocalPath)) {
                                    unlink($imgLocalPath);
                                    Mage::log('Prev Image is removed.  '.$fileName, null, $import_log_filename);
                                }

                                if (copy($imgUrl, $imgLocalPath)) { // php5+ only
                                    Mage::log('Image is copied OK.  '.$fileName, null, $import_log_filename);
                                }
                            } catch (Exception $ex) {
                                Mage::log($ex->getTraceAsString(), null, $import_log_filename);
                            }

                            /**
                             * @var Mage_Catalog_Model_Product $Product
                             */
                            if (!empty($imgLocalPath) && file_exists($imgLocalPath)) {
                                if (file_exists($dirMageMediaGalleryPath.DS.$fileName)) {
                                    Mage::log('Previous Image already attached to the Product.'."\n".$dirMageMediaGalleryPath.DS.$fileName, null, $import_log_filename);
                                    $mediaPathFileName = Mage_Core_Model_File_Uploader::getDispretionPath($fileName).DS.$fileName;
                                    Mage::log('Let`s remove it from Product.', null, $import_log_filename);
                                    $Product->removeImageFromMediaGallery($mediaPathFileName);
                                }

                                Mage::log('Add image to Media Gallery.', null, $import_log_filename);
                                try {
                                    $Product->addImageToMediaGallery($imgLocalPath, array('image', 'small_image', 'thumbnail'), /*move*/ false, /*exclude*/ false);
                                } catch (Exception $ex) {
                                    Mage::log( $ex->getTraceAsString(), null, $import_log_filename);
                                }

                            }
                        } else {
                            Mage::log('*** No IMAGE at: '.$imgUrl, null, $import_log_filename);
                        }
                    } else {
                        Mage::log('*** Article is empty! '.$item->saveXML(), null, $import_log_filename);
                    }
                } else {
                    Mage::log('*** No URL for image of product '.$sku, null, $import_log_filename);
                }

                /**
                 * Минимальное количество для заказа
                 */
                if (isset($item->attributes()->default_quantity) && !empty($item->attributes()->default_quantity)) {
                    $Product->setData('start_order_qty', intval($item->attributes()->default_quantity));
                } else {
                    $Product->setData('start_order_qty', 1);
                }

                /**
                 * Let's create categories
                 */
                $arrCategoriesIDs = array();
                $arrCategoriesIDs[] = $parentCategoryId = $store_default_category_id;

                if (isset($item->attributes()->class_name) && !empty($item->attributes()->class_name)) {
                    $category_path = strval($item->attributes()->class_name);
                    $md5catPath = md5($category_path);
                    if (!array_key_exists($md5catPath, $arrExistCategories)) {
                        $arrExistCategories[$md5catPath] = array();
                        Mage::log("Detected Category Path: ". $category_path , null, $import_log_filename);
                        if (preg_match('/|/', $category_path)) {
                            // categories tree
                            $category_path = explode('|',$category_path);

                            if (is_array($category_path) && !empty($category_path)) {
                                /**
                                 * Подготовим коллекцию для поиска по категориям
                                 */
                                $collection = Mage::getModel('catalog/category')
                                    ->setStoreId($store_id)
                                    ->getCollection()
                                    ->addFieldToFilter('parent_id', array('eq'=>$parentCategoryId))
                                    ->addAttributeToSelect('name')
                                    ->addAttributeToSelect('is_active');
                                // массив
                                foreach($category_path as $category_name) {
                                    $isCatExist = false;
                                    $category_name = trim($category_name);
                                    Mage::log('Let`s check Category "'.$category_name.'".', null, $import_log_filename);

                                    /**
                                     * Я ищу категорию по названию, и если нахожу, то беру её ID
                                     * а если не нахожу, то создаю её
                                     */
                                    foreach ($collection as $cat) {
                                        if ($cat->getName() == $category_name) {
                                            $parentCategoryId = $cat->getId();
                                            $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = (int)$parentCategoryId;
                                            $isCatExist = true;

                                            Mage::log('Category exist "'.$cat->getName().'". Parent ID now: '.$parentCategoryId, null, $import_log_filename);
                                            break;
                                        }
                                    }

                                    /**
                                     * There is NO category by name
                                     */
                                    if (!$isCatExist) {
                                        /**
                                         * Let's create it
                                         */
                                        $urlKey = strtolower( preg_replace('/[^\da-z]/i', '', $category_name) );
                                        Mage::log('URL key of non existent category: '.$urlKey, null, $import_log_filename);

                                        /**
                                         * Let's check if same URL_KEY exists
                                         */
                                        if (!empty($urlKey)) {
                                            $currentCategory = Mage::getModel('catalog/category')
                                                ->setStoreId($store_id)
                                                ->getCollection()
                                                ->addFieldToFilter('url_key', $urlKey)
                                                ->addFieldToFilter('parent_id', array('eq'=>$parentCategoryId))
                                                ->setCurPage(1)
                                                ->setPageSize(1)
                                                ->getFirstItem();

                                            /**
                                             * Если такого urlkey действительно нет,
                                             * то создадим категорию
                                             */
                                            if (!($currentCategory && $currentCategory->getId())) {

                                                Mage::log('No such URL key, let`s create category', null, $import_log_filename);

                                                $category = Mage::getModel('catalog/category');
                                                $category->setName($category_name)
                                                    ->setUrlKey($urlKey)
                                                    ->setIsActive(1)
                                                    ->setDisplayMode('PRODUCTS')
                                                    ->setIsAnchor(1)
                                                    //->setDescription('This is a '.$category_name.' subcategory')
                                                    //->setCustomDesignApply(1)
                                                    //->setCustomDesign('storybloks/new-category-style') // create this template and layout in your design directory
                                                    ->setAttributeSetId($category->getDefaultAttributeSetId())
                                                ;

                                                $parentCategory = Mage::getModel('catalog/category')->load($parentCategoryId);
                                                $category->setPath($parentCategory->getPath());

                                                Mage::log("parentCategory->getPath(): ".$parentCategory->getPath(), null, $import_log_filename);
                                                $category->save();

                                                $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = $parentCategoryId = $category->getId();

                                                Mage::log("Created category: ".$category_name.' with ID = '.$category->getId(), null, $import_log_filename);

                                                unset($category, $parentCategory);
                                            } else {
                                                /**
                                                 * А если такой urlKey существует...
                                                 */
                                                $arrExistCategories[$md5catPath][] = $arrCategoriesIDs[] = $parentCategoryId = $currentCategory->getId();
                                                Mage::log('urlKey exists '.$urlKey.', $currentCategory->getId() = '.$currentCategory->getId(), null, $import_log_filename);
                                            }
                                        } else {
                                            Mage::log('*** urlKey empty for category '.$category_name, null, $import_log_filename);
                                        }
                                    }
                                }

                                unset($collection);
                            }
                        } else {
                            // one category
                            Mage::log('There is only one category: '.$category_path.'.', null, $import_log_filename);
                        }
                    } else {
                        foreach($arrExistCategories[$md5catPath] as $catId) {
                            $arrCategoriesIDs[] = $catId;
                        }
                    }

//                    if (MY_DEBUG_MODE) Mage::log( 'Line: '.__LINE__, null, $import_log_filename);

                    $Product->setCategoryIds($arrCategoriesIDs);
                }

                $Product->save();
                unset($Product);
                $g++; // только полезные товары

                Mage::log( "Product saved as ".$sku, null, $import_log_filename); // DEBUG MODE

                /**
                 * Counters
                 */
                if ($id > 0) {
                    $u++;
                } else {
                    $i++;
                }
            }
        } else {
            Mage::log('XML error: unknown xml->items', null, $import_log_filename);
        }
    }
} catch (Exception $ex) {
    Mage::logException( $ex );
    Mage::log( 'Exception: see exception.log' );
}

$strTheEnd = 'Items imported: '.$i."\n".'Items updated: '.$u."\n".date('Y.m.d H:i:s')." DONE\n";
Mage::log($strTheEnd, null, $import_log_filename);

function get_url_contents($url)
{
    $crl = curl_init();
    $timeout = 5;
    curl_setopt ($crl, CURLOPT_URL, $url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    curl_close($crl);
    return $ret;
}

function logVar($item) {
    $out = '';
    $object = $item;
    if (is_object($object)) {
        $classNameOfObj = get_class($object);
        $out .= $classNameOfObj . "\n";
        $ms = get_class_methods($object);
        sort($ms);
        $out .= print_r($ms, true);
        $out .= "\n\n";
    } else {
        $out .= 'Not object' . "\n";
        if (is_array($object)) {
            $out .= 'Array:' . "\n";
            foreach ($object as $k => $v) {
                if (is_object($v)) {
                    $object[$k] = get_class($v);
                }
            }
            $out .= print_r($object, true) . "\n";
        } else {
            ob_start();
            var_dump($object);
            $vardump_out = ob_get_contents();
            ob_clean();
            $out .= $vardump_out . "\n";
        }
    }
    Mage::log($out);
}
