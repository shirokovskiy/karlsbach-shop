<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 18.04.17
 * Time         : 1:24
 * Description  :
 */


function getHtml($url, $post = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if(!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

$url = "https://aupontrouge.ru/en/aupontrouge/catalog_category/ajaxview/id/38/?limit=72";

try {
    $page = getHtml($url);
} catch (Exception $ex) {
    echo "Что-то пошло не так..".PHP_EOL;
    echo $ex->getTraceAsString();
}

echo $page.PHP_EOL;
//

if (//preg_match("/Leather Sneakers on contrast sole\./i", $page)
//    &&
preg_match("/\<div class=\"filterresult\"/i", $page)
) {
    echo "Всё плохо!".PHP_EOL;die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");

    $to  = "Dmitry Shirokovskiy <d.shirokovskiy@aupontrouge.ru>, ";
    $to .= "Magento Developer <jimmy.webstudio@gmail.com>";

    $subject = "APR Crawler Report | $logDate | ".date("H:i");

    $message = "АПР сайт в английской версии соскочил с реиндексации опять!";

    $headers  = "Content-type: text/html; charset=utf-8 \r\n";
    $headers .= "From: APR Web-Shop <Sales@em.aupontrouge.ru>\r\n";
//$headers .= "Bcc: birthday-archive@example.com\r\n";

    if (!mail($to, $subject, $message, $headers)) {
        echo "bad attempt to send email".PHP_EOL;
    }

} else {
    echo "OK!".PHP_EOL;
}

