-- Clear Categories
DELETE FROM `catalog_category_entity_varchar` WHERE `value_id` > 7;
ALTER TABLE `catalog_category_entity_varchar` AUTO_INCREMENT =8;
DELETE FROM `catalog_category_entity_int` WHERE `value_id` > 6;
ALTER TABLE `catalog_category_entity_int` AUTO_INCREMENT =7;
DELETE FROM `catalog_category_entity_text` WHERE `value_id` > 4;
ALTER TABLE `catalog_category_entity_text` AUTO_INCREMENT =5;
DELETE FROM `catalog_category_entity` WHERE `entity_id` > 2;
ALTER TABLE `catalog_category_entity` AUTO_INCREMENT =3;
