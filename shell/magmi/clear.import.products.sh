#!/usr/bin/env bash
#
# This file will start MAGMI import process
# mode=...
# update : will skip non existing skus & update existing ones
# create : will create new products for non exisiting skus, will update existing ones
# xcreate : will create new products for non exisiting skus, will skip existing ones
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
DR="$(pwd)"
cd $THIS_DIR

MSHELL=$DR/shell/magmi
DBUSER=karlsbach
DBNAME=karlsbach_shop
FILE=import_products.csv

if [ -f $DR/var/import/$FILE ]
then
    echo "Close WebShop!*!*!"
    touch $DR/maintenance.flag

    echo "Start CLEAR PRODUCTS"
    mysql --defaults-extra-file=$DR/shell/tools/custom.cnf $DBNAME < $MSHELL/clear.products.sql
#    echo "Start CLEAR CATEGORIES" Будь осторожен, т.к. есть и другие корневые директории складов
#    mysql --defaults-extra-file=$DR/shell/tools/custom.cnf $DBNAME < $MSHELL/clear.catalogs.sql

    echo "Start SQL-Helper"
    mysql --defaults-extra-file=$DR/shell/tools/custom.cnf $DBNAME < $MSHELL/import.helper.sql

    echo "Start IMPORT PRODUCTS"
    php $DR/magmi/cli/magmi.cli.php -mode=create

    echo "Backup IMPORTed PRODUCTS csv-file"
    mv -f $DR/var/import/$FILE $DR/var/import/import_products.csv.$(date +%Y-%m-%d__%H.%M).csv
else
    echo "No "$FILE" at "$(date "+%F %H:%M")
fi

bash $DR/rmcache.sh
echo "Open WebShop !!!"
rm $DR/maintenance.flag
echo "The End"
date
