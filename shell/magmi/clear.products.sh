#!/bin/bash
#
# This file will clear all products in Magento and Media files (such as pictures of products)
#
BDIR=/var/www/data/karlsbach
DR=$BDIR/htroot
MSHELL=$DR/shell/magmi
DBUSER=karlsbach
DBPASW=LRAyrU7NVAbR9
DBNAME=karlsbach_shop

# Clear all products information from DB
mysql -f -p$DBPASW -u $DBUSER $DBNAME < $MSHELL/clear.products.sql
rm -rf $DR/var/cache/

if [ -d $DR/media/catalog/product/placeholder ];
then
    mv -f $DR/media/catalog/product/placeholder $DR/media/catalog/.
else
    echo "Folder PLACEHOLDER not exists!"
fi

rm -rf $DR/media/catalog/product/*

if [ -d $DR/media/catalog/placeholder ];
then 
    mv -f $DR/media/catalog/placeholder $DR/media/catalog/product/.
fi

if [ -d $DR/media/tmp/catalog/product ];
then
	echo "Delete TMP catalog product images" 
    rm -rf $DR/media/tmp/catalog/product/*
fi