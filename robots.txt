User-agent: *
Disallow: /admin
Disallow: /app
Disallow: /skin
Disallow: /var
Disallow: /shell
Disallow: /downloader
Host: shop.karlsbach.eu
