<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (version_compare(phpversion(), '5.2.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.2.0 or newer.
<a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a>
 Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

/**
 * Error reporting
 */
error_reporting(E_ALL &~ E_STRICT &~ E_NOTICE &~ E_DEPRECATED);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile) && !preg_match('/admin/i', $_SERVER['REQUEST_URI'])) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

require_once $mageFilename;

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Varien_Profiler::enable();
    Mage::setIsDeveloperMode(true);
    ini_set('display_errors', 1);
}

/*******/
$myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '188.134.80.143' : $_SERVER['REMOTE_ADDR']; // Saint-Petersburg, Russia
//    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '66.249.78.29' : $_SERVER['REMOTE_ADDR']; // Beverly Hills, USA
//    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '144.76.166.251' : $_SERVER['REMOTE_ADDR']; // Kiez, Germany
//    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '81.7.14.169' : $_SERVER['REMOTE_ADDR']; // Hermsdorf, Germany
//    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '79.134.215.30' : $_SERVER['REMOTE_ADDR']; // Tallin, Estonia
//$myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '62.65.192.82' : $_SERVER['REMOTE_ADDR']; // Tallin, Estonia
$myIPfile = MAGENTO_ROOT.'/var/log/IP/'.$myIP.'.ip';

if (!file_exists($myIPfile)) {
    define( 'GEOIP_DAT', MAGENTO_ROOT.'/lib/GeoData/GeoIP.dat' );
    define( 'GEOIPCITY_DAT', MAGENTO_ROOT.'/lib/GeoData/GeoIPCity.dat' );

    include_once MAGENTO_ROOT.'/lib/GeoData/GeoIP/geoip.inc';
    include_once MAGENTO_ROOT.'/lib/GeoData/GeoIP/geoipcity.inc';
    include_once MAGENTO_ROOT.'/lib/GeoData/GeoIP/geoipregionvars.php';

    $objGeoIP = geoip_open(GEOIPCITY_DAT, GEOIP_STANDARD);
    $rcGeoIP = geoip_record_by_addr($objGeoIP, $myIP);

    if ( !is_null($rcGeoIP) ) {

        touch($myIPfile);

        if (isset($rcGeoIP->country_name)) {
            switch ($rcGeoIP->country_name) {
                case 'Russian Federation':
                    break;

                default:
                    $url = Mage::app()->getStore('europe')->getBaseUrl();
                    header('Location: '.$url); exit;
                    break;
            }
        }
    }

    geoip_close($objGeoIP);
}
/*******/

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';

switch($_SERVER['HTTP_HOST']) {
    case 'market.karlsbach.ru':
    case 'market.karlsbach.com':
    case 'market.karlsbach.eu':
    case 'market.karlsbach.ee':
    case 'market.karlsbach.du':
        $mageRunCode = 'rusretailstore';
        $mageRunType = 'store';
        break;
}

Mage::run($mageRunCode, $mageRunType);
